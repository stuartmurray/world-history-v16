﻿// JScript File
var index = 0;
cBInteractive.ClickRevealStar.Init = function () {

    $('.click_area').live('click', function () {
        index = $(this).attr('index');
        $('.click_area1').removeClass('click_area');
        $('.map_area').removeClass('zoom_map');
        $('.over_close').removeClass('btnClose1');
        $(".popup").show();
        $('.popup .tab').hide();
        $('.popup .tab' + index + '').show();
    });


    $('.toolTipClose').live('click', function () {
        $(".popup").hide();
        $('.click_area1').addClass('click_area');
        $('.map_area').addClass('zoom_map');
        $('.over_close').addClass('btnClose1');
    });


    $('.zoom_map').live('click', function () {
        $('.overlay, .overlay1').show();
    });

    $('.btnClose1').live('click', function () {
        $(".overlay, .overlay1").hide();
        $('.click_area1').addClass('click_area');
        $('.map_area').addClass('zoom_map');
    });


}

$(function () {
    cBInteractive.ClickRevealStar.Init();
});