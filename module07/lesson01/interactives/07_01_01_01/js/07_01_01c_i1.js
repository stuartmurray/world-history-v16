﻿// JScript File

cBInteractive.MultiChoice.Init = function(){
	$('.slide_0').show();
    showApplet();
};

var totalDrag = 0;
var xStart = 0;
var xDelta = 0;
var startVal = 86;
var scrollEnd = 656;
var dragObj;

var optionLeftArr = [126,168,214,255,300,343,387,430,474,517,560,604];
var optionLeftArrMid = [146,192,234,278,320,366,408,451,494,537,580,656];
var optionLeftArrStart = [86,146,192,234,278,320,366,408,451,494,537,580];


function showApplet(){
	dragObj = document.getElementById('slidingButton');
	if(navigator.platform=='iPad'||navigator.platform=='iPhone'){
		dragObj.ontouchstart=function(event){
			event.preventDefault();
			var t = event.targetTouches[0];
			xStart = parseInt(t.pageX);
			dragObj.style.left = parseInt(getStyle(dragObj,'left')) + 'px';
			document.ontouchmove=scrolling;
			document.ontouchend=endDrag;
		}
	
	}else{
		dragObj.onmousedown=function(event){
			event = window.event || event;
			event.preventDefault ? event.preventDefault() : event.cancelBubble = true;
			//event.preventDefault();
			xStart = parseInt(event.clientX);
			dragObj.style.left = parseInt(getStyle(dragObj,'left')) + 'px';
			document.onmousemove=scrolling;
			document.onmouseup=endDrag;
		}
		
	}
}

function getStyle(oElm, strCssRule){
	var strValue = "";
	if(document.defaultView && document.defaultView.getComputedStyle){
		var css = document.defaultView.getComputedStyle(oElm, null);
		strValue = css ? css.getPropertyValue(strCssRule) : null;
	}else if(oElm.currentStyle){
		strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1){
			return p1.toUpperCase();
		});
		strValue = oElm.currentStyle[strCssRule];
	}
	return strValue;
}

function scrolling(event){
	event = window.event || event;
	event.preventDefault ? event.preventDefault() : event.cancelBubble = true;
	if(navigator.platform=='iPad'||navigator.platform=='iPhone'){
		var t = event.targetTouches[0];		
		touchX = t.pageX;
		xDelta = xStart - parseInt(touchX);
		xStart = parseInt(touchX);
		totalDrag = parseInt(dragObj.style.left) - xDelta;
		if(totalDrag <= startVal || totalDrag >= scrollEnd){
			totalDrag=null;
		}else{
			dragObj.style.left = totalDrag + 'px';
			dragObj.style.zIndex = 11;
		}
	}else{
		xDelta = xStart - parseInt(event.clientX);
		xStart = parseInt(event.clientX);
		totalDrag = parseInt(dragObj.style.left) - xDelta;
		if(totalDrag <= startVal || totalDrag >= scrollEnd){
			totalDrag=null;
		}else{
			dragObj.style.left = totalDrag + 'px';
			dragObj.style.zIndex = 11;
			
		}
	}
}

function endDrag(){
	if(navigator.platform=='iPad'||navigator.platform=='iPhone'){
		document.ontouchmove=null;
		document.ontouchend=null;
	}else{
		document.onmouseup=null;
		document.onmousemove=null;
	}
	var img_index = getIndexPosition();
	changeScrollImg(img_index);
}

function getIndexPosition(){
	var leftTmp = dragObj.style.left;
	var left = leftTmp.replace('px','');
	if(left < optionLeftArrMid[0] && left > optionLeftArrStart[0]){
		dragObj.style.left = optionLeftArr[0] + "px";
		return 0;
	}else if(left < optionLeftArrMid[1] && left > optionLeftArrStart[1]){
		dragObj.style.left = optionLeftArr[1] + "px";
		return 1;
	}else if(left < optionLeftArrMid[2] && left > optionLeftArrStart[2]){
		dragObj.style.left = optionLeftArr[2] + "px";
		return 2;
	}else if(left < optionLeftArrMid[3] && left > optionLeftArrStart[3]){
		dragObj.style.left = optionLeftArr[3] + "px";
		return 3;
	}else if(left < optionLeftArrMid[4] && left > optionLeftArrStart[4]){
		dragObj.style.left = optionLeftArr[4] + "px";
		return 4;
	}else if(left < optionLeftArrMid[5] && left > optionLeftArrStart[5]){
		dragObj.style.left = optionLeftArr[5] + "px";
		return 5;
	}else if(left < optionLeftArrMid[6] && left > optionLeftArrStart[6]){
		dragObj.style.left = optionLeftArr[6] + "px";
		return 6;
	}else if(left < optionLeftArrMid[7] && left > optionLeftArrStart[7]){
		dragObj.style.left = optionLeftArr[7] + "px";
		return 7;
	}else if(left < optionLeftArrMid[8] && left > optionLeftArrStart[8]){
		dragObj.style.left = optionLeftArr[8] + "px";
		return 8;
	}else if(left < optionLeftArrMid[9] && left > optionLeftArrStart[9]){
		dragObj.style.left = optionLeftArr[9] + "px";
		return 9;
	}else if(left < optionLeftArrMid[10] && left > optionLeftArrStart[10]){
		dragObj.style.left = optionLeftArr[10] + "px";
		return 10;
	}else if(left < optionLeftArrMid[11] && left > optionLeftArrStart[11]){
		dragObj.style.left = optionLeftArr[11] + "px";
		return 11;
	}
/*	else if(left < optionLeftArrMid[12] && left > optionLeftArrStart[12]){
		dragObj.style.left = optionLeftArr[12] + "px";
		return 12;
	}else if(left < optionLeftArrMid[13] && left > optionLeftArrStart[13]){
		dragObj.style.left = optionLeftArr[13] + "px";
		return 13;
	}else if(left < optionLeftArrMid[14] && left > optionLeftArrStart[14]){
		dragObj.style.left = optionLeftArr[14] + "px";
		return 14;
	}
*/	else{
		dragObj.style.left = optionLeftArr[0] + "px";
		return 0;
	}
}

function changeScrollImg(ind){
	$('.slide').hide();
	$('.slide_'+ind+'').show();
}

cBInteractive.MultiChoice.Init();