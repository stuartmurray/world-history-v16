﻿var count = 1;
var currentSlide = 0;
$(document).ready(function () {

    $('#imgPlay').on('click', function () {
        $('.playerOver').css('display', 'none');
        //        $('#mep_1').find('.mejs-play').find('button').trigger('click'); //start audio player mep_1 - audiobutton is 0
        check = 1;
        $('#mep_4').find('.mejs-play').find('button').trigger('click'); //start audio player mep_1 - audiobutton is 0
        currentSlide = 1;
    });

    $('.ui-navigation .next').bind('click', function () {
        $('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking
        setTimeout(function () {
            $('.ui-navigation .next').css("visibility", "visible");
        }, 400);
        playAudio();
    });
    $('.ui-navigation .prev').bind('click', function () {
        $('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking
        setTimeout(function () {
            $('.ui-navigation .prev').css("visibility", "visible");
        }, 400);
        playAudio();
    });
    $('.ui-navigation .restart').bind('click', function () {
        currentSlide = 1;
        count = 1;
        playAudio();
    });
});

function playAudio() {
    if (count == 0 && currentSlide == 0) { // not started yet
    } else {
        slideNumber();
        var icount = 0;
        if (count == 1) {
            icount = 4;
        } else if (count == 2) {
            icount = 5;
        } else if (count == 3) {
            icount = 6;
        } else if (count == 4) {
            icount = 7;
        }
        check = 1;
        $('#mep_' + (icount)).find('.mejs-mute').mouseover();
        //		$('#mep_' + (count)).find('.audioSlide').attr("src", "interactives/07_04_03/07_04_03a/audio/07_04_screen03_0"+currentSlide+".mp3");
        $('#mep_' + (icount)).find('.audioSlide').attr("src", "audio/07_04_screen03_0" + currentSlide + ".mp3");
        $('#mep_' + (icount)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");
        $('#mep_' + (icount)).find('.mejs-play').find('button').trigger('click');
    }
}

function slideNumber() {
    var step1 = $('.ui-navigation-tracker').html(); // 3 of 10
    var step2 = step1.indexOf(" of"); // 1
    var step3 = step1.substr(0, step2);
    currentSlide = parseInt(step3);
    count = currentSlide;
}

var check = 0;
$('._audio-tip').click(function () {
    check = 1;
    for (var i = 0; i < $("audio").length; i++) {
        $("audio")[i].pause();
    }
    $('.tooltip').hide();
    $('#tooltip_' + $(this).attr('id')).show();
    //    console.log('show');
});
$(document).click(function (e) {
    if (check == 1) {
        check = 0;
        return;
    }
    var $target = $(e.target);
    if ($target.closest('.tooltip').length == 0) {
        //        console.log('hide');
        for (var i = 0; i < $("audio").length; i++) {
            $("audio")[i].pause();
        }
        $('.tooltip').hide();
    }
});
