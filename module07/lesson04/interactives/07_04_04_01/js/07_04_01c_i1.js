﻿// JScript File

Init = function () {


    $(".tab").find('.nxt').click(function () {
        var oCurrSet = $(this).closest(".tab").find(".set:visible");
        $(oCurrSet).next(".set").show();
        $(oCurrSet).hide();
        $(this).closest(".tab").find(".prev").removeAttr("disabled").css({ 'opacity': '1', 'filter': 'alpha(opacity = 100)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)', 'cursor': 'pointer' }).attr({ 'title': 'Previous' });

        if ($(oCurrSet).next(".set").next(".set").length == 0) {
            $(this).closest(".tab").find(".nxt").attr("disabled", "true").css({ 'opacity': '0.3', 'filter': 'alpha(opacity = 30)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)', 'cursor': 'default' }).removeAttr('title');
        }
        $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) + 1) + " of " + ($(this).closest(".tab").find(".set").length));

    });


    $(".tab").find('.prev').click(function () {
        var oCurrSet = $(this).closest(".tab").find(".set:visible");
        $(this).closest(".tab").find(".nxt").removeAttr("disabled").css({ 'opacity': '1', 'filter': 'alpha(opacity = 100)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)', 'cursor': 'pointer' }).attr({ 'title': 'Next' });
        $(oCurrSet).prev(".set").show();
        $(oCurrSet).hide();
        if ($(oCurrSet).prev(".set").prev(".tab .set").length == 0) {
            $(this).closest(".tab").find(".prev").attr("disabled", "true").css({ 'opacity': '0.3', 'filter': 'alpha(opacity = 30)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)', 'cursor': 'default' }).removeAttr('title');
        }
        $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) + 1) + " of " + ($(this).closest(".tab").find(".set").length));

    });

    showApplet();

}

var totalDrag = 0;
var xStart = 0;
var xDelta = 0;
var startVal = 17;
var scrollEnd = 690;
var dragObj;

var optionLeftArr = [33, 63, 95, 127, 157, 187, 219, 249, 280, 311, 342, 372, 403, 435, 466, 496, 528, 557, 589, 619, 651, 681];
var optionLeftArrMid = [47, 83, 116, 142, 170, 203, 235, 266, 297, 330, 356, 385, 420, 445, 480, 517, 543, 572, 599, 635, 666, 690];
var optionLeftArrStart = [17, 47, 83, 116, 142, 170, 203, 235, 266, 297, 330, 356, 385, 420, 445, 480, 517, 543, 572, 599, 635, 666];

var scrollimg_txt = [];

var captionText = []

var scroll_img = [];

function showApplet() {
    dragObj = document.getElementById('slidingButton');
    if (navigator.platform == 'iPad' || navigator.platform == 'iPhone') {
        dragObj.ontouchstart = function (event) {
            event.preventDefault();
            var t = event.targetTouches[0];
            xStart = parseInt(t.pageX);
            dragObj.style.left = parseInt(getStyle(dragObj, 'left')) + 'px';
            document.ontouchmove = scrolling;
            document.ontouchend = endDrag;
        }

    } else {
        dragObj.onmousedown = function (event) {
            event = window.event || event;
            event.preventDefault ? event.preventDefault() : event.cancelBubble = true;
            //event.preventDefault();
            xStart = parseInt(event.clientX);
            dragObj.style.left = parseInt(getStyle(dragObj, 'left')) + 'px';
            document.onmousemove = scrolling;
            document.onmouseup = endDrag;
        }

    }
}

function getStyle(oElm, strCssRule) {
    var strValue = "";
    if (document.defaultView && document.defaultView.getComputedStyle) {
        var css = document.defaultView.getComputedStyle(oElm, null);
        strValue = css ? css.getPropertyValue(strCssRule) : null;
    } else if (oElm.currentStyle) {
        strCssRule = strCssRule.replace(/\-(\w)/g, function (strMatch, p1) {
            return p1.toUpperCase();
        });
        strValue = oElm.currentStyle[strCssRule];
    }
    return strValue;
}

function scrolling(event) {
    event = window.event || event;
    event.preventDefault ? event.preventDefault() : event.cancelBubble = true;
    if (navigator.platform == 'iPad' || navigator.platform == 'iPhone') {
        var t = event.targetTouches[0];
        touchX = t.pageX;
        xDelta = xStart - parseInt(touchX);
        xStart = parseInt(touchX);
        totalDrag = parseInt(dragObj.style.left) - xDelta;
        if (totalDrag <= startVal || totalDrag >= scrollEnd) {
            totalDrag = null;
        } else {
            dragObj.style.left = totalDrag + 'px';
            dragObj.style.zIndex = 11;
        }
    } else {
        xDelta = xStart - parseInt(event.clientX);
        xStart = parseInt(event.clientX);
        totalDrag = parseInt(dragObj.style.left) - xDelta;
        if (totalDrag <= startVal || totalDrag >= scrollEnd) {
            totalDrag = null;
        } else {
            dragObj.style.left = totalDrag + 'px';
            dragObj.style.zIndex = 11;

        }
    }
}

function endDrag() {
    if (navigator.platform == 'iPad' || navigator.platform == 'iPhone') {
        document.ontouchmove = null;
        document.ontouchend = null;
    } else {
        document.onmouseup = null;
        document.onmousemove = null;
    }
    var img_index = getIndexPosition();
    changeScrollImg(img_index);
}

function getIndexPosition() {

    var leftTmp = dragObj.style.left;
    var left = leftTmp.replace('px', '');
    if (left < optionLeftArrMid[0] && left > optionLeftArrStart[0]) {
        dragObj.style.left = optionLeftArr[0] + "px";
        return 0;
    } else if (left < optionLeftArrMid[1] && left > optionLeftArrStart[1]) {
        dragObj.style.left = optionLeftArr[1] + "px";
        return 1;
    } else if (left < optionLeftArrMid[2] && left > optionLeftArrStart[2]) {
        dragObj.style.left = optionLeftArr[2] + "px";
        return 2;
    } else if (left < optionLeftArrMid[3] && left > optionLeftArrStart[3]) {
        dragObj.style.left = optionLeftArr[3] + "px";
        return 3;
    } else if (left < optionLeftArrMid[4] && left > optionLeftArrStart[4]) {
        dragObj.style.left = optionLeftArr[4] + "px";
        return 4;
    } else if (left < optionLeftArrMid[5] && left > optionLeftArrStart[5]) {
        dragObj.style.left = optionLeftArr[5] + "px";
        return 5;
    } else if (left < optionLeftArrMid[6] && left > optionLeftArrStart[6]) {
        dragObj.style.left = optionLeftArr[6] + "px";
        return 6;
    } else if (left < optionLeftArrMid[7] && left > optionLeftArrStart[7]) {
        dragObj.style.left = optionLeftArr[7] + "px";
        return 7;
    } else if (left < optionLeftArrMid[8] && left > optionLeftArrStart[8]) {
        dragObj.style.left = optionLeftArr[8] + "px";
        return 8;
    } else if (left < optionLeftArrMid[9] && left > optionLeftArrStart[9]) {
        dragObj.style.left = optionLeftArr[9] + "px";
        return 9;
    } else if (left < optionLeftArrMid[10] && left > optionLeftArrStart[10]) {
        dragObj.style.left = optionLeftArr[10] + "px";
        return 10;
    } else if (left < optionLeftArrMid[11] && left > optionLeftArrStart[11]) {
        dragObj.style.left = optionLeftArr[11] + "px";
        return 11;
    } else if (left < optionLeftArrMid[12] && left > optionLeftArrStart[12]) {
        dragObj.style.left = optionLeftArr[12] + "px";
        return 12;
    } else if (left < optionLeftArrMid[13] && left > optionLeftArrStart[13]) {
        dragObj.style.left = optionLeftArr[13] + "px";
        return 13;
    } else if (left < optionLeftArrMid[14] && left > optionLeftArrStart[14]) {
        dragObj.style.left = optionLeftArr[14] + "px";
        return 14;
    } else if (left < optionLeftArrMid[15] && left > optionLeftArrStart[15]) {
        dragObj.style.left = optionLeftArr[15] + "px";
        return 15;
    } else if (left < optionLeftArrMid[16] && left > optionLeftArrStart[16]) {
        dragObj.style.left = optionLeftArr[16] + "px";
        return 16;
    } else if (left < optionLeftArrMid[17] && left > optionLeftArrStart[17]) {
        dragObj.style.left = optionLeftArr[17] + "px";
        return 17;
    } else if (left < optionLeftArrMid[18] && left > optionLeftArrStart[18]) {
        dragObj.style.left = optionLeftArr[18] + "px";
        return 18;
    } else if (left < optionLeftArrMid[19] && left > optionLeftArrStart[19]) {
        dragObj.style.left = optionLeftArr[19] + "px";
        return 19;
    } else if (left < optionLeftArrMid[20] && left > optionLeftArrStart[20]) {
        dragObj.style.left = optionLeftArr[20] + "px";
        return 20;
    } else if (left < optionLeftArrMid[21] && left > optionLeftArrStart[21]) {
        dragObj.style.left = optionLeftArr[21] + "px";
        return 21;
    } else {
        dragObj.style.left = optionLeftArr[0] + "px";
        return 0;
    }
}

function changeScrollImg(ind) {
    $('.slide').hide();
    $('.slide_' + ind + '').show();
}

$(function () {
    Init();
});

var check = 0;
$('._audio-tip').click(function () {
    check = 1;
    for (var i = 0; i < $("audio").length; i++) {
        $("audio")[i].pause();
    }
    $('.tooltip').hide();
    $('#tooltip_' + $(this).attr('id')).show();
    //    console.log('show');
});
$(document).click(function (e) {
    if (check == 1) {
        check = 0;
        return;
    }
    var $target = $(e.target);
    if ($target.closest('.tooltip').length == 0) {
        //        console.log('hide');
        for (var i = 0; i < $("audio").length; i++) {
            $("audio")[i].pause();
        }
        $('.tooltip').hide();
    }
});