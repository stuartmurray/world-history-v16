﻿// JScript File
var enaSubmit = false;
Init = function () {
    var arr_grp = [[],
['textbox2', 'textbox3', 'textbox4', 'textbox5', 'textbox6', 'textbox7', 'textbox8', 'textbox9'],
['textbox13', 'textbox14', 'textbox15', 'textbox16', 'textbox17', 'textbox18', 'textbox19', 'textbox20', 'textbox21', 'textbox22'],
['textbox56', 'textbox57', 'textbox58', 'textbox59', 'textbox60', 'textbox61', 'textbox62', 'textbox63', 'textbox64', 'textbox65', 'textbox66', 'textbox67', 'textbox68', 'textbox69', 'textbox70', 'textbox71', 'textbox72', 'textbox73'],
['textbox115', 'textbox116', 'textbox117', 'textbox118', 'textbox119', 'textbox120', 'textbox121', 'textbox122', 'textbox123', 'textbox124'],
['textbox128', 'textbox129', 'textbox130', 'textbox131', 'textbox132', 'textbox133', 'textbox134', 'textbox135', 'textbox136', 'textbox137', 'textbox138', 'textbox139'],
['textbox143', 'textbox144', 'textbox145', 'textbox146', 'textbox147', 'textbox148', 'textbox149', 'textbox150', 'textbox151'],
[],
["textbox1", "textbox4", "textbox11", "textbox15", "textbox24", "textbox30", "textbox36", "textbox42"],
["textbox10", "textbox12", "textbox23", "textbox29", "textbox35", "textbox41", "textbox48", "textbox55", "textbox73"],
["textbox17", "textbox25", "textbox31", "textbox37", "textbox43", "textbox50", "textbox62", "textbox76", "textbox83", "textbox90", "textbox97", "textbox104", "textbox109"],
["textbox19", "textbox26", "textbox32", "textbox38", "textbox44", "textbox51", "textbox64", "textbox77", "textbox84", "textbox91", "textbox98"],
["textbox27", "textbox33", "textbox39", "textbox46", "textbox53", "textbox69", "textbox79", "textbox86", "textbox93", "textbox100"],
["textbox28", "textbox34", "textbox40", "textbox47", "textbox54", "textbox71", "textbox80", "textbox87", "textbox94", "textbox101", "textbox106", "textbox111"],
["textbox45", "textbox52", "textbox66", "textbox78", "textbox85", "textbox92", "textbox99", "textbox105", "textbox110", "textbox113", "textbox119", "textbox126", "textbox133", "textbox141", "textbox150", "textbox153"],
["textbox49", "textbox57", "textbox74", "textbox81", "textbox88", "textbox95", "textbox102", "textbox107", "textbox112", "textbox114", "textbox125", "textbox127", "textbox140", "textbox142", "textbox152"],
["textbox59", "textbox75", "textbox82", "textbox89", "textbox96", "textbox103", "textbox108"]
]

    var ans_arr = ["", "TRENCHES", "PROPAGANDA", "SELF-DETERMINATION", "BLITZKRIEG", "ISOLATIONISM", "INFLATION", "", "GENOCIDE", "RADIATION", "ANTI-SEMITISM", "APPEASEMENT", "MILITARISM", "TOTALITARIAN", "DEMILITARIZATION", "DEMOCRATIZATION", "FASCISM"];


    var selected_arr = null;
    var ptr = 0;
    var attempt = 0;
    var n = 0;

    var completecount = 0;
    //var checkans=0;
    $('.crosswordleft input').attr('disabled', 'disabled');
    $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
    $('.cross-bot-btn').addClass('hidee').removeAttr('title');
    $('.crosswordleft input').addClass('textbox'); // To make inputbox disabled when popup is open
    function initcross() {
        selected_arr = null;
        ptr = 0;
        attempt = 0;
        n = 0;
        completecount = 0
        $('.crosswordleft input').attr('disabled', 'disabled').removeAttr('readonly');
        $('.cross-bot-btn').addClass('hidee').removeAttr('title');
        $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
        $('.done').removeAttr('style');
        $('.done').removeClass('done');
        $('.active').removeClass('active')
        $('.lockked').removeClass('lockked').removeClass('bggreen');
        $('.crosstxtarea').css('cursor', 'pointer');
        for (var i = 0; i < arr_grp.length; i++) {
            for (var j = 0; j < arr_grp[i].length; j++) {
                $('.' + arr_grp[i][j]).val('');
            }
        }

    }

    $('.crosstxtarea').hover(function () {
        if ($(this).css('cursor') == "pointer")
            $(this).css({ "background": "#EAF7FF" });
    }, function () {
        if ($(this).css('cursor') == "pointer")
            $(this).css({ "background": "#d6eefe" });
    });

    $('.crosstxtarea').click(function () {
        if ($(this).css('cursor') == "pointer") {
            var numVal = $(this).find('.no').html();
            $('.cross_numbers span').show();
            $('.num_' + numVal + '').hide();
            enaSubmit = false;
            //n=$(this).index();
            n = $(this).attr('index');
            selected_arr = arr_grp[n];
            if (document.onkeypress == null)
                document.onkeypress = keyinput
            if (!($(this).hasClass('done') || $(this).hasClass('active'))) {
                enaSubmit = true;
                disableOtherInputs();
                focusInput();

                n = $(this).attr('index');
                selected_arr = arr_grp[n];

                attempt = 0;
                $('.active').removeClass('active');
                $(this).addClass('active');
                if (isFilled() && $('.popup-bg').css('display') == "none" && enaSubmit) {
                    $('.cross-check').removeClass('disabledd').css({ 'cursor': 'pointer' }).attr('title', 'Check');
                } else {
                    $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
                }
            }
        }
    });

    function disableOtherInputs() {
        $('.crosswordleft input:not(".bggreen")').attr('disabled', 'disabled');
        for (i = 0; i < selected_arr.length; i++) {
            //if(!($('.'+selected_arr[i]).hasClass('bggreen'))){
            $('.' + selected_arr[i]).removeAttr('disabled');
            //}
        }
    }

    function focusInput() {
        var cnt = 0;
        while (cnt < selected_arr.length) {
            if (!($('.' + selected_arr[cnt]).hasClass('lockked'))) {
                $('.' + selected_arr[cnt]).focus(); // To trigger keypad in ipad
                ptr = cnt;
                break;
            } else {
                cnt++;
            }
        }
    }

    $('.crosswordleft input').focus(function () {
        if ($(this).hasClass('bggreen')) {
            $(this).blur();
        }
        var classNam = $(this).attr('class');
        classNam = classNam.replace(" textbox", "");
        var classInd = $.inArray(classNam, selected_arr);
        ptr = classInd;
        document.onkeypress = keyinput;
    });

    function keyinput(e) {
        var evtobj = window.event ? event : e //distinguish between IE's explicit event object (window.event) and Firefox's implicit.
        var unicode = evtobj.charCode ? evtobj.charCode : evtobj.keyCode

        var actualkey = String.fromCharCode(unicode);
        if (unicode == 9) {
            if (ptr == selected_arr.length - 1) {
                ptr = 0;
                givefirstFocus();
                return false;
            }
        }
        if ((unicode >= 65 && unicode <= 90) || (unicode >= 97 && unicode <= 122) || (unicode == 9 || unicode == 46 || unicode == 8) || (unicode == 45))
        { }
        else {
            $('.popup-bg, .popup-innerbg-number, .overlay').show();
            return false;
        }

        if (actualkey >= 'a' && actualkey <= 'z') {
            actualkey = actualkey.toUpperCase();
        }
        if (actualkey >= 'A' && actualkey <= 'Z' && (selected_arr != null) || actualkey == '-') {
            if (!$('.' + selected_arr[ptr]).hasClass('lockked')) {
                $('.' + selected_arr[ptr]).val(actualkey);
                //alert(selected_arr.length +'---'+ ptr);
                if (ptr != selected_arr.length - 1) {
                    if ($('.' + selected_arr[ptr + 1]).hasClass('lockked') == true) {
                        $('.' + selected_arr[ptr + 2]).focus();
                    } else {
                        $('.' + selected_arr[ptr]).blur();
                        $('.' + selected_arr[ptr + 1]).focus();
                        //alert(selected_arr[ptr+1]);
                        /*$('.'+selected_arr[ptr]).blur(function(){
                        $('.'+selected_arr[ptr+1]).focus();
                        });*/

                    }
                } else {
                    ptr = 0;
                    givefirstFocus();
                }
            }

            if (isFilled() && $('.popup-bg').css('display') == "none" && enaSubmit) {
                $('.cross-check').removeClass('disabledd').css({ 'cursor': 'pointer' }).attr('title', 'Check');
            }

        }
    }

    function givefirstFocus() {
        while (ptr < selected_arr.length) {
            if (!$('.' + selected_arr[ptr]).hasClass('lockked')) {
                $('.' + selected_arr[ptr]).focus();
                break;
            } else {
                ptr++;
            }
        }
    }

    $('.cross-check').click(function () {
        if (!$('.cross-check').hasClass('disabledd')) {
            $('.crosswordleft .textbox').addClass('lockked'); // To make inputbox disabled when popup is open
            attempt++;
            var str = "";
            for (var i = 0; i < selected_arr.length; i++) {
                str = str + $('.' + selected_arr[i]).val();
            }
            if (str == ans_arr[n]) {
                $('.active').css('opacity', '0.5')
                $('.active').css('cursor', 'default');
                $('.active').addClass('done');
                var cluenum = $('.active').attr('clueno'); // to turn the clues red
                $('.clues li').eq(cluenum).css('color', 'red')

                $('.done').removeClass('active')
                $('.popup-bg').show();
                $('.popup-innerbg-correct').show();
                $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
                completecount++;
                attempt = 0;
                setAllLocked();
                if (completecount >= 15) {
                    $('.cross-bot-btn').removeClass('hidee').attr('title', 'Restart');
                }
                else if (completecount >= 7) {
                    $('.showentireans').show();
                    $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
                }
            } else {
                if (attempt >= 2) {
                    $('.popup-bg').show();
                    $('.popup-innerbg-showans').show();
                    $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
                } else {
                    $('.popup-bg').show();
                    $('.popup-innerbg-try').show();
                    $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
                }
            }
        }
    })
    function isFilled() {
        for (var i = 0; i < selected_arr.length; i++) {
            if ($('.' + selected_arr[i]).val() == "")
                return false;
        }
        return true;
    }


    $('.close-small-bt').click(function () {
        $('.crosswordleft .textbox').removeClass('lockked'); 	// To make inputbox disabled when popup is open					
        $(this).parent().parent().hide();
        $(this).parent().parent().parent().hide();
        $('.popup-bg, .pop_over, .overlay').hide();
        for (var i = 0; i < selected_arr.length; i++) {
            $('.' + selected_arr[i]).addClass('lockked').removeClass('textbox');
        }
        selected_arr = null;
    });

    $('.close_num').click(function () {
        $('.popup-bg, .popup-innerbg-number, .overlay').hide();
    });

    $('.tryagain-bt').click(function () {
        $('.crosswordleft .textbox').removeClass('lockked');
        $('.popup-bg').hide();
        $('.popup-innerbg-try').hide();
        focusInput(); // To trigger keypad in ipad	
    });

    $('.reset').click(function () {
        $('.crosswordleft .textbox').removeClass('lockked');
        $('.popup-bg').hide();
        $('.popup-innerbg-try').hide();
        for (var i = 0; i < selected_arr.length; i++) {
            if (!($('.' + selected_arr[i]).hasClass('lockked'))) {
                $('.' + selected_arr[i]).val('');
            }
        }
        focusInput(); // To trigger keypad in ipad	
    });

    $('.showans-bt').click(function () {
        $('.cross_numbers span').show();
        $('.crosswordleft .textbox').removeClass('lockked'); // To make inputbox disabled when popup is open				
        $('.popup-bg').hide();
        $('.popup-innerbg-showans').hide();
        $('.active').css('opacity', '0.5')
        $('.active').css('cursor', 'default');
        $('.active').addClass('done');
        var cluenum = $('.active').attr('clueno'); // to turn the clues red
        $('.clues li').eq(cluenum).css('color', 'red')
        $('.done').removeClass('active')
        for (var i = 0; i < selected_arr.length; i++) {
            $('.' + selected_arr[i]).val(ans_arr[n].substr(i, 1));
            $('.' + selected_arr[i]).addClass('lockked').addClass('bggreen').attr('readonly', 'readonly').removeClass('textbox');
        }

        $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
        completecount++;
        if (completecount >= 15) {
            $('.cross-bot-btn').removeClass('hidee').attr('title', 'Restart');
        } else if (completecount >= 7) {
            $('.showentireans').show();
            $('.cross-check').addClass('disabledd').css({ 'cursor': 'auto' }).removeAttr('title');
        }
        selected_arr = null;
    })
    $('.cross-bot-btn').click(function () {
        if (!$('.cross-bot-btn').hasClass('hidee'))
            $('.clues li').css('color', '#444444');
        $('.crosswordleft input').addClass('textbox');
        initcross();
    });
    function setAllLocked() {
        for (var i = 0; i < selected_arr.length; i++) {
            $('.' + selected_arr[i]).addClass('lockked').addClass('bggreen').attr('readonly', 'readonly');
        }
    }

    $('#hint').live('click', function () {
        $('#content_sction, .overlay').show();

    });

    $('#close').live('click', function () {
        $('#content_sction, .overlay').hide();
    });

    $('.showentireans').live('click', function () {
        $(this).hide();
        $('.cross-bot-btn').removeClass('hidee').attr('title', 'Restart');
        $('.clues li').css('color', 'red');
        $('.crosstxtarea').each(function () {
            n = $(this).index();
            selected_arr = arr_grp[n];
            for (var i = 0; i < selected_arr.length; i++) {
                $('.' + selected_arr[i]).val(ans_arr[n].substr(i, 1));
                $('.' + selected_arr[i]).addClass('lockked').removeAttr('disabled').attr('readonly', 'readonly');
            }
        });
        $('.crosstxtarea').css('cursor', 'default');
    });


};

$(function () {
    Init();
});