﻿$(document).ready(function () {
    $('.greenImg, .greenText').bind('click', function () {
        //alert(0);
        $('.greenImg').addClass('imgGreen');
        $('.greenText').removeAttr('cursor', 'pointer');
        $('.greenText').css('cursor', 'default');
        $('.left_li').addClass('commonUnhide');
        //$(this).addClass('bordergreen');        
    });
    $('.darkgreenImg, .darkgreenText').bind('click', function () {
        //alert(0);
        $('.darkgreenImg').addClass('imgDarkgreen');
        $('.darkgreenText').removeAttr('cursor', 'pointer');
        $('.darkgreenText').css('cursor', 'default');
        $('.middle_li').addClass('commonUnhide');
        //$(this).addClass('bordergreen');        
    });
    $('.redImg, .redText').bind('click', function () {
        //alert(0);
        $('.redImg').addClass('imgRed');
        $('.redText').removeAttr('cursor', 'pointer');
        $('.redText').css('cursor', 'default');
        $('.right_li').addClass('commonUnhide');
        //$(this).addClass('bordergreen');        
    });
});