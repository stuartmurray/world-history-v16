﻿$(document).ready(function () {
    $('.drop').on('drop', function () {
        $(this).droppable('disable').css('opacity', '1');
    });
    $('.ui_reset').on('click', function () {
        $('.drop').each(function () {
            $(this).droppable('enable');
        });
    });

    $('.ui_ddremove').on('click', function () {
        setTimeout(function () {
            $('.drop').each(function () {
                if (!$(this).find('li').length > 0) {
                    $(this).droppable('enable');
                //    alert(1);
                } else {
                //    alert(2);
                //    alert($(this).parent().parent().parent());
                  
                }
            });
        }, 500);
    });
});