﻿// JScript File

Init = function(){

$('.swfObject').css({"height":"auto"});

 $(".tab").find('.nxt').click(function(){ 
		$(".prev").css('cursor','pointer');		
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   $(oCurrSet).next(".set").show();
	   $(oCurrSet).hide();
		$(this).closest(".tab").find(".prev").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)'});
		resetAllDrop();
	   if($(oCurrSet).next(".set").next(".set").length==0){
		$(this).closest(".tab").find(".nxt").attr("disabled", "true");	
		$(this).closest(".tab").find(".nxt").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	;

	   }
	 	//pageNo = 1;
	//  pageNo++;
	   
	$(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
			
});
 
 	$(".tab").find('.prev').click(function(){
		$(".nxt").css('cursor','pointer');		
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	  $(this).closest(".tab").find(".nxt").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)'});
	   $(oCurrSet).prev(".set").show();
	   $(oCurrSet).hide();
	   resetAllDrop();
	   if($(oCurrSet).prev(".set").prev(".tab .set").length==0){
		   $(this).closest(".tab").find(".prev").attr("disabled", "true");	
		   		   $(this).closest(".tab").find(".prev").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	

	   }
	  // pageNo--;
	   
		$(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
		
 });
	
	
	

// ############# drag drop5  start ##############


(function($) {
$.fn.randomize = function(childElem) {
  return this.each(function() {
      var $this = $(this);
      //alert($this.html());
      //var elems = $this.children(childElem);
      var elems = $this.find('.'+childElem);
      //alert($(elems).length);
      elems.sort(function() { return (Math.round(Math.random())-0.5); });  

      //$this.remove(childElem);
      //alert(elems.length);
      //alertArrayHTML(elems);  
      for(var i=0; i < elems.length; i++){
        //alert($($this.children(childElem)).eq(i).html());
        //alert($(elems[i]).html())
        $($this.find('.'+childElem)).eq(i).replaceWith($(elems[i]).clone());
        //$this.append(elems[i]);      
      }  
  });    
}
})(jQuery);

var isDragDropTried5=false;


	$(".resetDrag1").hide(); 
	
	if($.inArray(navigator.platform, ["iPhone","iPad"]) != -1) {
		dragiPad5(".CheckDragBox5");
		dropiPad5(".UserAnswer5");
	}
	else {
		dragWeb5(".CheckDragBox5");
    	dropWeb5(".UserAnswer5");
	}
	
	
    $(".dragDropContent5 .showAnsBtn").click(function(){
	  $(this).closest(".dragDropContent5").find(".DragDropShowAns").hide(); 
	  $(this).closest(".dragDropContent5").find(".btnpos").find(".resetDrag1").show(); 
	     $(this).closest(".dragDropContent5").find(".removeDrag").show();
		var oCurrActivity5;
		oCurrActivity5 = $(this).closest(".dragDropContent5");
		var oSwapList5;
		oSwapList5 = document.createElement("div")
		$("body").append(oSwapList5);
		$(oSwapList5).hide();
		$(oCurrActivity5).find(".wrong").each(function(){
		   $(oSwapList5).append($(this).closest(".CheckDragBox5"));
		   $(oSwapList5).find(".CheckDragBox5").each(function(){
		        var sGroup5 = $(this).attr("id").substring(7,13);
		        $(this).find(".iconPos").remove();
		  $(this).append('<img src="imgs/right.png" alt="" class="iconPos"/>');
		        $(oCurrActivity5).find(".UserAnswer5[id*='"+ sGroup5 +"']").append($(this));
		   });
		});
		$(oSwapList5).remove();
     });
	
	
 $(".resetDrag, .resetDrag1").click(function(){				
			resetAllDrop();
     });
 
 
 function resetAllDrop (){
		 $(".dragDropContent5").find(".CheckMyWork").show().addClass("Opacity10").css({"cursor":"default"});
		 $(".dragDropContent5").find(".CheckMyWork").unbind("click");
		 $(".dragDropContent5").find('.resetDrag1, .goodjob').hide();
		 $(".dragDropContent5").find(".removeDrag").hide(); 
		 $('.dropBgPos5').randomize('CheckDragBox5');
		 $(".dragDropContent5").find(".CheckDragBox5 img").remove();      //icon removed
			 // 	dragiPad5(".CheckDragBox5");
				dragWeb5(".CheckDragBox5");
				
				if($.inArray(navigator.platform, ["iPhone","iPad"]) != -1) {
					dragiPad5(".CheckDragBox5");
				}
				
			$(".dragDropContent5").find(".stopDrag").remove();
			 isDragDropTried5=false; 
 }
	
    
    $(".dragDropContent5 .tryAgainBtn").click(function(){
        var sGroup5, oWrong5;
        isDragDropTried5=true;

          $(this).closest(".dragDropContent5").find(".DragDropTryAgain").hide();
		   $(this).closest(".dragDropContent5").find(".removeDrag").hide();
        // **** revert wrong answer
        $(this).closest(".dragDropContent5").find(".UserAnswer5").each(function(){
            sGroup5 = $(this).attr("id").substring(11,17);
            if ($(this).find(".CheckDragBox5").length>0){
                if ($(this).find(".CheckDragBox5").attr("id").indexOf(sGroup5)==-1){
                    oWrong5=$(this).find(".CheckDragBox5");
					$(this).find(".CheckDragBox5").find(".iconPos").remove();      //icon removed
                    $(this).closest(".dragDropContent5").find(".CheckDragBox5").each(function(){
                        if ($(this).find(".CheckDragBox5").length!=0){
                            $(this).append(oWrong5);
							$(this).find(".CheckDragBox5").find(".iconPos").remove();      //icon removed
							$(this).closest(".dragDropContent5").find(".CheckMyWork").show().addClass("Opacity10").css({"cursor":"default"});

							dragiPad5(".CheckDragBox5");
						}						
						
                    });
                }
                sGroup5="";
            }
			
        });
    });
	


function DragDropValidate5(draggable){
    var isCorrect5, sGroup5;
		
    isCorrect5 = true;
    $(draggable).closest(".dragDropContent5").find(".UserAnswer5").each(function(){
       sGroup5 = $(this).attr("id").substring(11,17);
		
        if ($(this).find(".CheckDragBox5").length>0){
            if ($(this).find(".CheckDragBox5").attr("id").indexOf(sGroup5)==-1){
               $(this).find(".CheckDragBox5").find(".iconPos").remove();
				$(this).find(".CheckDragBox5").append('<img src="imgs/wrong.png" alt="" class="iconPos wrong"/>');
                isCorrect5 = false;
            }
			else{
			    $(this).find(".CheckDragBox5").find(".iconPos").remove();
				$(this).find(".stopDrag").remove();
				$(this).find(".CheckDragBox5").append('<img src="imgs/right.png" alt="" class="iconPos"/>');
				$(this).find(".CheckDragBox5").parent().append('<div class="stopDrag"></div>');
				
				//$(this).find(".CheckDragBox5").removeClass("ui-draggable");
				//$(this).find(".CheckDragBox5").css({"position":"static"})
			}
			
            sGroup5="";

        }
    });

    if (isCorrect5){
         $(draggable).closest(".dragDropContent5").find(".goodjob").show();
         $(draggable).closest(".dragDropContent5").find(".CheckMyWork").addClass("Opacity10");
        $(draggable).closest(".dragDropContent5").find(".CheckMyWork").unbind("click");
        $(".resetDrag").show();
		  $(draggable).closest(".dragDropContent5").find(".CheckMyWork, resetDrag1").hide();
    }
    else{
        if(isDragDropTried5){
             $(draggable).closest(".dragDropContent5").find(".DragDropShowAns").show();
             $(draggable).closest(".dragDropContent5").find(".CheckMyWork").hide();
            $(draggable).closest(".dragDropContent5").find(".UserAnswer5").each(function(){
                sGroup5 = $(this).attr("id").substring(11,17);
                if ($(this).find(".CheckDragBox5").length>0){
                    if ($(this).find(".CheckDragBox5").attr("id").indexOf(sGroup5)==-1){
                        $(this).find(".CheckDragBox5").addClass("CheckDragBoxDisable");
                    }
                    sGroup5="";
                }
            });
            // ****      
        }
        else{
            $(draggable).closest(".dragDropContent5").find(".CheckMyWork").unbind("click");   
            $(draggable).closest(".dragDropContent5").find(".DragDropTryAgain").show();  
		    $(draggable).closest(".dragDropContent5").find(".CheckMyWork").addClass("Opacity10");
        }
    }
}

function dragWeb5(sFilter){
	$(sFilter).draggable({revert:true});
    $(sFilter).bind("dragstart", function(event, ui) {
        $(this).css("z-index","1002");
    });
}

function dragWeb5Disable(sFilter){
	$(sFilter).draggable("disable");
}


function dropWeb5(sFilter){
	$(sFilter).droppable({ 
        drop: function(event, ui){
		        DoDrop5(ui.draggable,$(this));
        } 
    });
}


function dragiPad5(sFilter){
	$(sFilter).each(function(){
		 new webkit_draggable(this,{scroll:false, revert: 'always' });
        $(this).bind("touchstart", function(event, ui) {
	   $(this).css("z-index","1001");
	
						
		});
	});
}


function RedragiPad5(oObj5){
		 new webkit_draggable(oObj5,{scroll:false, revert: 'always' });
        $(this).bind("touchstart", function(event, ui) {
			    $(this).css("z-index","1001");
		});
}

function dragiPad5Disable(){
	webkit_draggable.destroy();
}

function dropiPad5(sFilter){
	$(sFilter).each(function(){
	    webkit_drop.add(this, {onDrop : function(dragged,dropped){
	           if ($.trim($(webkit_drop.dropped).text())!=""){
	                DoDrop5(dragged,$(webkit_drop.dropped));
					RedragiPad5(dragged);
	            }
			//	else{
						
				//	}
			}
		});
			
	});
}

function DoDrop5(draggable,droppable){
 var oSwap;

		oSwap=$(droppable).find(".CheckDragBox5");
		
	$(droppable).append(draggable); 
    $(draggable).css("top","-10px");
    $(draggable).css("left","0px");       
    $(draggable).css("z-index","auto"); //because in drag start z-index is 1001
    $(draggable).animate( {top:"0px"}, 300, "linear", function(){
        $(this).css("top","0px");
        $(this).css("left","0px");
		 $(this).css("margin-top","0px");
    });
$(".UserAnswer5").each(function(){
		if($(this).find(".CheckDragBox5").length==0){
			$(this).append(oSwap);
			//dragiPad5(oSwap);
			}

});
  CheckDragDrop5(draggable);
}



function CheckDragDrop5(draggable){
	var i=0;
	$(draggable).closest(".dragDropContent5").find(".CheckDragBox5").each(function(index){
	i = i + $(this).find(".CheckDragBox5").length;
	});

    if (i== 0) {
        $(draggable).closest(".dragDropContent5").find(".CheckMyWork").removeClass("Opacity10").css({"cursor":"pointer"});
		$(draggable).closest(".dragDropContent5").find(".CheckMyWork").removeClass("disabled");
        $(draggable).closest(".dragDropContent5").find(".CheckMyWork").click(function(){
         DragDropValidate5(draggable);
		 $(draggable).closest(".dragDropContent5").find(".CheckMyWork").css({"cursor":"default"});
		  $(draggable).closest(".dragDropContent5").find(".removeDrag").show();
        });
    }	 
}


// ############# drag drop end ##############

 }
 Init();