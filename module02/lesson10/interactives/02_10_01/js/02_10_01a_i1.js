﻿// JScript File
var animateFlag = true;
Init = function(){

$(".tab").find('.nxt').click(function(){ 
		clearTime();
	   $(".prev").css('cursor','pointer');
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   $(oCurrSet).next(".set").show();
	   $(oCurrSet).hide();
		$(this).closest(".tab").find(".prev").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)'});
	   if($(oCurrSet).next(".set").next(".set").length==0){
		   	animateFlag = false;
		$(this).closest(".tab").find(".nxt").attr("disabled", "true").css('cursor','auto');
		   $(this).closest(".tab").find(".nxt").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	
	   }
	$(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
	InitAudio($(".ClickRevealStar").find(".set:visible").find(".SSAudio").attr("href"));
});
 
 $(".tab").find('.prev').click(function(){
		clearTime();
	   $(".nxt").css('cursor','pointer');
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   $(this).closest(".tab").find(".nxt").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)'});
	   $(oCurrSet).prev(".set").show();
	   $(oCurrSet).hide();
	   if($(oCurrSet).prev(".set").prev(".tab .set").length==0){
		   $(this).closest(".tab").find(".prev").attr("disabled", "true");
		   $(this).closest(".tab").find(".prev").removeAttr("title");
		   $(this).closest(".tab").find(".prev").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	
	   }
		$(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
		InitAudio($(".ClickRevealStar").find(".set:visible").find(".SSAudio").attr("href"));
 });
 
 
 
 
 $('.clk1').live('click', function(){
			$('.clk1Txt, .popup').show();
			$('.clk2Txt, .clk3Txt').hide();
		  });
	
$('.clk2').live('click', function(){
			$('.clk2Txt, .popup').show();
			$('.clk1Txt, .clk3Txt').hide();
		  });
	
$('.clk3').live('click', function(){
			$('.clk3Txt, .popup').show();
			$('.clk2Txt, .clk1Txt').hide();
		  });

 $('.toolTipClose').click(function(){  
			 $('.popup, .clk1Txt, .clk2Txt, .clk3Txt').hide();
});	
 
 
$(".tab").find(".startbtn").click(function(){
				$(".startbtn, .playTxt").hide();
				$(".container_bg").hide();
				//$("#jquery_jplayer_in_1").jPlayer("play");
				
		InitAudio($(".ClickRevealStar").find(".set:visible").find(".SSAudio").attr("href"));
				
});
$(".tab").find(".replaybtn").click(function(){
				clearTime();
				animateFlag = true;
				$(".replaybtn, .replayTxt").hide();
				$(".container_bg, .set:visible").hide();
				$('.set:first').show();
				$('.pageNo').html('1/12');
				$(".nxt").removeAttr("disabled").css({'cursor':'pointer'});
				$(".nxt").attr({'title':'Next'});
				$(".prev").attr("disabled", "true").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});
/*		InitAudio($(".ClickRevealStar").find(".set:visible").find(".SSAudio").attr("href"));
*/				//$("#jquery_jplayer_in_1").jPlayer("play");
						});
 


	
}

InitAudio=function(sAudio){
	
    $("#jquery_jplayer_in_1").jPlayer("destroy");
    $("#jquery_jplayer_in_1").jPlayer({
	    ready: function (event) {
		    $(this).jPlayer("setMedia", {
			    mp3:sAudio
		    })//.jPlayer("play");
	    },
	    play: function() {
			$(this).jPlayer("pauseOthers");
	    },
		ended: function() {

		var oCurrSet = $(".tab").find(".set:visible");
		if($(oCurrSet).next(".set").length==0){
					$(".replaybtn, .replayTxt").show();
					$(".container_bg").show();
					}	
			if(animateFlag){
				animateNext();
			}


		},
	    swfPath: "../global/js",
	    supplied: "mp3",
	    preload: 'metadata',
	    cssSelectorAncestor: "#jp_container-in_1",
	    cssSelector: {
          play: '.jp-play-in',
          pause: '.jp-pause-in',
          mute: '.jp-mute-in',
          unmute: '.jp-unmute-in'
         },
	    wmode: "window"
    }); 
}



var time1 = 0;
var time2 = 0;
var time3 = 0;
var time4 = 0;
var time5 = 0;
var time6 = 0;
var time7 = 0;
var time8 = 0;
var time9 = 0;
var time10 = 0;
var time11 = 0;
var time12 = 0;
function animateNext(){
	time1 = setTimeout(function(){$(".nxt").css({'opacity':'0.5','filter':'alpha(opacity = 50)'});},300);
	time2 = setTimeout(function(){$(".nxt").css({'opacity':'1','filter':'alpha(opacity = 100)'});},600);
	time3 = setTimeout(function(){$(".nxt").css({'opacity':'0.5','filter':'alpha(opacity = 50)'});},900);
	time4 = setTimeout(function(){$(".nxt").css({'opacity':'1','filter':'alpha(opacity = 100)'});},1200);
	time5 = setTimeout(function(){$(".nxt").css({'opacity':'0.5','filter':'alpha(opacity = 50)'});},1500);
	time6 = setTimeout(function(){$(".nxt").css({'opacity':'1','filter':'alpha(opacity = 100)'});},1800);
	time7 = setTimeout(function(){$(".nxt").css({'opacity':'0.5','filter':'alpha(opacity = 50)'});},2100);
	time8 = setTimeout(function(){$(".nxt").css({'opacity':'1','filter':'alpha(opacity = 100)'});},2400);
	time9 = setTimeout(function(){$(".nxt").css({'opacity':'0.5','filter':'alpha(opacity = 50)'});},2700);
	time10 = setTimeout(function(){$(".nxt").css({'opacity':'1','filter':'alpha(opacity = 100)'});},3000);
	time11 = setTimeout(function(){$(".nxt").css({'opacity':'0.5','filter':'alpha(opacity = 50)'});},3300);
	time12 = setTimeout(function(){$(".nxt").css({'opacity':'1','filter':'alpha(opacity = 100)'});},3600);
}


function clearTime(){
	$(".nxt").css({'opacity':'1','filter':'alpha(opacity = 100)'});
clearTimeout(time1);
clearTimeout(time2);
clearTimeout(time3);
clearTimeout(time4);
clearTimeout(time5);
clearTimeout(time6);
clearTimeout(time7);
clearTimeout(time8);
clearTimeout(time9);
clearTimeout(time10);
clearTimeout(time11);
clearTimeout(time12);
}

 

Init (); 
 
/*cBInteractive.ClickRevealStar.InitAudio=function(sAudio){
    $("#jquery_jplayer_in_1").jPlayer("destroy");
    $("#jquery_jplayer_in_1").jPlayer({
	    ready: function (event) {
		    $(this).jPlayer("setMedia", {
			    mp3:sAudio
		    }).jPlayer("play");
	    },
	    play: function() {
		    $(this).jPlayer("pauseOthers");
	    },
	    swfPath: "../global/js",
	    supplied: "mp3",
	    preload: 'metadata',
	    cssSelectorAncestor: "#jp_container-in_1",
	    cssSelector: {
          play: '.jp-play-in',
          pause: '.jp-pause-in',
          mute: '.jp-mute-in',
          unmute: '.jp-unmute-in'
         },
	    wmode: "window"
    }); 
}
 
*/
