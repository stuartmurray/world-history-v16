$(document).ready(function () {
    $('.restart').on('click', function () {
        $('.tabs').find('.tabs-block').find('.tab').each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
              
            }
        });
       
        $('.tabs').find('.tabs-block').find('.tab:first').addClass('active');
        $('.panel').each(function () {
            $(this).addClass('notVisible');
        });
        $('.panel:first').removeClass('notVisible');
    });

    $('.prev').on('click', function () {
        $('.tabs').find('.tabs-block').find('.tab').each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
               
            }
        });
       
        $('.tabs').find('.tabs-block').find('.tab:first').addClass('active');
        $('.panel').each(function () {
            $(this).addClass('notVisible');
        });
        $('.panel:first').removeClass('notVisible');
    });

    $('.next').on('click', function () {
        $('.tabs').find('.tabs-block').find('.tab').each(function () {
            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
               
            }
        });
       
        $('.tabs').find('.tabs-block').find('.tab:first').addClass('active');

        $('.tab2').find('.panel:first').removeClass('notVisible');
    });
});