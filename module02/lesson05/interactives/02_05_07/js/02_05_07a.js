﻿$(document).ready(function () {
    $('.customPopDiv').hide();

    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
    });

    $('.area').unbind('click').bind('click', function () {
        $('.customPopDiv').show();

        var getId = $(this).attr('id');
        if (getId == 'area1') {
            $('.customPopDiv ').css("height", "153px");
            $('.customPopDiv .head1').html('Building Wide Upper Stories');
            $('.customPopDiv .content1').html('To save space, builders built up and made the upper stories of buildings wider than the bottom floors. Artisans generally lived and worked in the same building. Their homes-often town homes with multiple floors-doubled as workshops. The shop itself occupied the first, or ground, floor. The master and his family lived on the floor above. Journeymen and apprentices (usually just one of each) lived on the topmost floor or in the attic. Sometimes they might even sleep at the back of the shop.');
        } else if (getId == 'area2') {
            $('.customPopDiv ').css("height", "81px");
            $('.customPopDiv .head1').html("Traveling Merchants");
            $('.customPopDiv .content1').html('Merchants traveled from town to town with goods to sell. They could often be seen moving in caravans, or groups, through the streets with goods loaded onto pack animals and into carts and wagons.');

        } else if (getId == 'area3') {
            $('.customPopDiv ').css("height", "113px");
            $('.customPopDiv .head1').html('Legions of Artisans');
            $('.customPopDiv .content1').html('By the High Middle Ages, a variety of artisans could be found throughout the urban centers of Europe. Cobblers made shoes, armourers made weapons, tanners prepared leather, and weavers made textiles. These products were sold directly from their workshops-increasingly for money rather than for barter.');
        } else if (getId == 'area4' || getId == 'area5') {
            $('.customPopDiv ').css("height", "113px");
            $('.customPopDiv .head1').html('Farm Products');
            $('.customPopDiv .content1').html('The city was never far from the country. Outside the towns and cities, many peasants still worked the fields of manorial estates. Even as feudal manors began to decline, poor people remained tied to the land. People brought livestock, such as sheep, goats and pigs, and crops directly from the farms to the cities to sell or trade.');
        }
        else if (getId == 'area6') {
            $('.customPopDiv ').css("height", "164px");
            $('.customPopDiv .head1').html('Working Stonemasons');
            $('.customPopDiv .content1').html("Stonemasons designed and built the cathedrals, castles and other buildings of the era. Most buildings contained some stonework, even just in their base. Many masons moved from town to town to work on different projects. Master masons generally oversaw all the work on a construction project, and the masons' guild became one of the most powerful of the era. Over time, the masons' guild developed a secret society, known as the Freemasons, that still exists today. The world's largest Masonic temple is located in Detroit, Michigan!");
        }
    });

});