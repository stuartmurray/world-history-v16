﻿// JScript File
var dragRevert = true;
var rightdrop = 0;

var feedTxt = ["<span class='neg_txt1'>That's not right! Please try again.</span> <img src='imgs/tryagain_btn.png' class='try_again' />","<span class='pos_txt'>Excellent! You have got all correct!</span>"]

Init = function(){
	InitDrag();
	
function InitDrag(){
	$(".ui-draggable").draggable({containment:'.contentMiddleBg', stack: ".drag_ul li" , revert: function(){if(dragRevert == true){return true;}else{dragRevert = true;}}});
	
	if(/iphone|ipod|ipad|android/i.test(navigator.userAgent)){
		init();
}

}
	
	$(".drop_ul>li").each(function(){
		$(this).droppable({
			drop:function(event, ui){
				if($(this).html() == ""){
					var dragid = ui.draggable.attr("id");
					var dropid = $(this).attr('id');
					$('#'+dragid).css({'left':'0px','top':'0px','zIndex':'0'});				
					if($('#'+dragid).attr('dragid') == $('#'+dropid).attr('dropid')){
						$(this).html(ui.draggable);
						$('#'+dragid).removeClass('ui-draggable').draggable("disable").css("opacity",1);
						rightdrop++;
						//alert(rightdrop);
						if(rightdrop == 12){
							$('.overlay').show();
							$('.feedback').html(feedTxt[1]).show();
						}
					}
					else{
						showFeedBack();
					}
				}
			}
		});
	});


function showFeedBack(){
	$('.overlay').show();
	$('.feedback').html(feedTxt[0]).show();
}



$('.try_again').live('click', function(){
	$('.overlay, .feedback').empty().hide();
});



function init(){
	var a=0;
	$(".drag_ul div>li").each(function(){
		var b=$(this).attr("id");
		var c=document.getElementById(b);
		a++;
		c.addEventListener("touchstart",touchHandler,true);
		c.addEventListener("touchmove",function(e){e.preventDefault();},true);
		c.addEventListener("touchmove",touchHandler,true);
		c.addEventListener("touchend",touchHandler,true);
		c.addEventListener("touchcancel",touchHandler,true);
	});
}

function touchHandler(a){
	var b=a.changedTouches,c=b[0],d="";
	switch(a.type){
		case"touchstart":
			d="mousedown";
			break;
		case"touchmove":
			d="mousemove";
			break;
		case"touchend":
			d="mouseup";
			break;
		default:
			return
	}
	var e=document.createEvent("MouseEvent");
	e.initMouseEvent(d,true,true,window,1,c.screenX,c.screenY,c.clientX,c.clientY,false,false,false,false,0,null);
	c.target.dispatchEvent(e);
}
};
Init ();

