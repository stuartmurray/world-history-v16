$(document).ready(function () {
    $('.drop').bind('drop', function () {
		if($(this).attr('data-label')) {
			if($(this).find('.bin').find('li').length == 2) {
				$(this).droppable('disable').css('opacity', '1');
			}
		}
    });
    $('.ui_reset, .ui_ddremove').bind('click', function () {
        $('.drop').each(function () {
            $(this).droppable('enable');                        
        });
    });
});