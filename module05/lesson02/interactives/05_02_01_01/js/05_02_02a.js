﻿var count = 0;

$(document).ready(function () {
    $('.hiddentool').hide();
    $('.hiddentool1').hide();
    $('.hiddentool2').hide();
    $('.hiddentool3').hide();
    $('.hiddentool4').hide();

    $(".aud").bind('click', function () {
        $('.hiddentool').show();
        $('.close2').bind('click', function () {
            $('.hiddentool').hide();
            if ($('#mep_0 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_0 button').trigger('click');
            }
        });
    });
    $(".aud1").bind('click', function () {
        $('.hiddentool1').show();
      
        $('.close2').bind('click', function () {
            $('.hiddentool1').hide();

            if ($('#mep_2 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_2 button').trigger('click');
            }
        });
    });
    $(".aud2").bind('click', function () {
        $('.hiddentool2').show(); 
        $('.close2').bind('click', function () {
            $('.hiddentool2').hide();

            if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_1 button').trigger('click');
            }
        });
    });

    $(".aud3").bind('click', function () {
        $('.hiddentool3').show();
        $('.close2').bind('click', function () {
            $('.hiddentool3').hide();

            if ($('#mep_3 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_3 button').trigger('click');
            }
        });
    });
    $(".aud4").bind('click', function () {
        $('.hiddentool4').show();
        $('.close2').bind('click', function () {
            $('.hiddentool4').hide();

            if ($('#mep_4 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_4 button').trigger('click');
            }
        });
    });

    $('.carousel-prev').bind('click', function () {
        $('.hiddentool').hide();
        $('.hiddentool1').hide();
        $('.hiddentool2').hide();
        $('.hiddentool3').hide();
        $('.hiddentool4').hide();
        if ($('#mep_0 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_0 button').trigger('click');
        }
        else if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_1 button').trigger('click');
        }
        else if ($('#mep_2 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_2 button').trigger('click');
        }
        else if ($('#mep_3 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_3 button').trigger('click');
        }
        else if ($('#mep_4 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_4 button').trigger('click');
        }
    });

    $('.carousel-next').bind('click', function () {
        $('.hiddentool').hide();
        $('.hiddentool1').hide();
        $('.hiddentool2').hide();
        $('.hiddentool3').hide();
        $('.hiddentool4').hide();
        if ($('#mep_0 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_0 button').trigger('click');
        }
        else if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_1 button').trigger('click');
        }
        else if ($('#mep_2 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_2 button').trigger('click');
        }
        else if ($('#mep_3 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_3 button').trigger('click');
        }
        else if ($('#mep_4 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_4 button').trigger('click');
        }
    });
    $('.ui-icon-closethick').bind('click', function () {
        $('.tooltip').hide();
        if ($('.tooltip #mep_0 .mejs-button').hasClass('mejs-pause')) {
            $('.tooltip #mep_0 button').trigger('click');
        }
       else if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_1 button').trigger('click');
        }
        else if ($('#mep_2 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_2 button').trigger('click');
        }
        else if ($('#mep_3 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_3 button').trigger('click');
        }
        else if ($('#mep_4 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_4 button').trigger('click');
        }
        $('.hiddentool').hide();
        $('.hiddentool1').hide();
        $('.hiddentool2').hide();
        $('.hiddentool3').hide();
        $('.hiddentool4').hide();
        
    });
    

});