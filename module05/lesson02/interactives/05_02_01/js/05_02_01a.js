var count = 0;
$(document).ready(function (e) {
    $('.ui-dialog-titlebar-close').on('click', function () {
        $('.tooltip').fadeOut('slow');
    });

    $('.hiddenglobal').hide();
    $(".aud").bind('click', function () {
        disableHide1();

        var audid = $(this).attr("id");
        audid = audid.slice(7, 8);
        $('.hiddentool' + audid).show();
    });
    $('.ui-dialog-titlebar-close').bind('click', function () {
        $('.hiddenglobal').hide();
        disableHide1();
    });
});

function disableHide1() {

    $('.hiddenglobal').hide();

    $('.mejs-button').each(function () {
        if ($(this).hasClass('mejs-pause')) {
            $(this).find('button').trigger('click');
        }
    });
}

var check = 0;
$('._audio-tip').click(function () {
    check = 1;
    for (var i = 0; i < $("audio").length; i++) {
        $("audio")[i].pause();
    }
    $('.tooltip').hide();
    $('#tooltip_' + $(this).attr('id')).show();
    //    console.log('show');
});
$(document).click(function (e) {
    if (check == 1) {
        check = 0;
        return;
    }
    var $target = $(e.target);
    if ($target.closest('.tooltip').length == 0) {
        //        console.log('hide');
        for (var i = 0; i < $("audio").length; i++) {
            $("audio")[i].pause();
        }
        $('.tooltip').hide();
    }
});