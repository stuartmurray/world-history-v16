﻿var count = 0;

var currentSlide = 0;

$(document).ready(function () {


    $('#imgPlay').live('click', function () {

        $('.playerOver').css('display', 'none');

        $('#mep_4').find('.mejs-play').find('button').trigger('click');

        currentSlide = 1;

    });



    $('.ui-navigation .next').bind('click', function () {

        $('.ui-navigation .next').css("visibility", "hidden"); //slow down clicking

        setTimeout(function () {

            $('.ui-navigation .next').css("visibility", "visible");

        }, 400);

        playAudio();

    });

    $('.ui-navigation .prev').bind('click', function () {

        $('.ui-navigation .prev').css("visibility", "hidden"); //slow down clicking

        setTimeout(function () {

            $('.ui-navigation .prev').css("visibility", "visible");

        }, 400);

        playAudio();

    });

    $('.ui-navigation .restart').bind('click', function () {

        currentSlide = 1;

        count = 0;

        playAudio();

    });

});


var icount = 0;
function playAudio() {

    if (count == 0 && currentSlide == 0) { // not started yet

    } else {

        slideNumber();
        if (count == 0) {
            icount = 4;
        } else if (count == 1) {
            icount = 5;
        } else if (count == 2) {
            icount = 6;
        } else if (count == 3) {
            icount = 7;
        } else if (count == 4) {
            icount = 8;
        } else if (count == 5) {
            icount = 9;
        } else if (count == 6) {
            icount = 10;
        }

        $('#mep_' + (icount)).find('.mejs-mute').mouseover();

        $('#mep_' + (icount)).find('.audioSlide').attr("src", "audio/guillotine_1.mp3");

        $('#mep_' + (icount)).find('.mejs-time-float-current').attr("setCurrentTime", "00:00");

        $('#mep_' + (icount)).find('.mejs-play').find('button').trigger('click');

    }

}



function slideNumber() {

    var step1 = $('.ui-navigation-tracker').html(); // 3 of 10

    var step2 = step1.indexOf(" of"); // 1

    var step3 = step1.substr(0, step2);

    currentSlide = parseInt(step3);

    count = currentSlide - 1;

}

var check = 0;
$('._audio-tip').click(function () {
    check = 1;
    for (var i = 0; i < $("audio").length; i++) {
        $("audio")[i].pause();
    }
    $('.tooltip').hide();
    $('#tooltip_' + $(this).attr('id')).show();
    //    console.log('show');
});
$(document).click(function (e) {
    if (check == 1) {
        check = 0;
        return;
    }
    var $target = $(e.target);
    if ($target.closest('.tooltip').length == 0) {
        //        console.log('hide');
        for (var i = 0; i < $("audio").length; i++) {
            $("audio")[i].pause();
        }
        $('.tooltip').hide();
    }
});