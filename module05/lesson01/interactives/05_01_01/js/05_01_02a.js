﻿var count = 0;

$(document).ready(function () {
    $('.customPopDiv').hide();
    $('.labels li').unbind('click').bind('click', function () {
        if (count == 1) {
            return;
        }
        count = 1;
        $('.labels li').css('cursor', 'default');
        $(this).css('color', 'red');
        var getId = $(this).attr('id');
        if (getId == 'austria_txt') {

            $('.customPopDiv .head1').html('<b>Austria:</b> 8.4 million; Parliamentary republic');

        } else if (getId == 'belarus_txt') {

            $('.customPopDiv .head1').html("<b>Belarus:</b> 9.5 million; Presidential republic");

        } else if (getId == 'belgium_txt') {

            $('.customPopDiv .head1').html("<b>Belgium:</b> 11 million; Constitutional monarchy");

        } else if (getId == 'bosnia_txt') {

            $('.customPopDiv .head1').html("<b>Bosnia/ Herzegovina:</b> 4 million; Parliamentary republicc");

        } else if (getId == 'bulgaria_txt') {

            $('.customPopDiv .head1').html("<b>Bulgaria:</b> 7.6 million; Parliamentary republic");

        }
        else if (getId == 'croatia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Croatia:</strong> 4.6 million; Parliamentary republic</p>');

        } else if (getId == 'cyprus_txt') {

            $('.customPopDiv .head1').html('<p><strong>Cyprus:</strong> 0.86 million; Presidential republic</p>');

        } else if (getId == 'czech_txt') {

            $('.customPopDiv .head1').html('<p><strong>Czech Rep:</strong> 10.5 million; Parliamentary republic</p>');

        } else if (getId == 'denmark_txt') {

            $('.customPopDiv .head1').html('<p><strong>Denmark:</strong> 5.6 million; Constitutional monarchy</p>');

        } else if (getId == 'estonia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Estonia:</strong> 1.3 million; Parliamentary republic</p>');

        } else if (getId == 'finland_txt') {

            $('.customPopDiv .head1').html('<p><strong>Finland:</strong> 5.4 million; Republic</p>');

        } else if (getId == 'france_txt') {

            $('.customPopDiv .head1').html('<p><strong>France:</strong> 65.4 million; Republic</p>');

        } else if (getId == 'georgia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Georgia:</strong> 4.4 million; Presidential republic</p>');

        } else if (getId == 'germany_txt') {

            $('.customPopDiv .head1').html('<p><strong>Germany:</strong> 81.8 million; Parliamentary republic</p>');

        } else if (getId == 'greece_txt') {

            $('.customPopDiv .head1').html('<p><strong>Greece:</strong> 11.6 million; Parliamentary republic</p>');

        } else if (getId == 'hungary_txt') {

            $('.customPopDiv .head1').html('<p><strong>Hungary:</strong> 9.9 million; Parliamentary republic</p>');

        } else if (getId == 'iceland_txt') {

            $('.customPopDiv .head1').html('<p><strong>Iceland:</strong> 0.304 million; Parliamentary republic</p>');

        } else if (getId == 'ireland_txt') {

            $('.customPopDiv .head1').html('<p><strong>Ireland:</strong> 4.4 million; Parliamentary republic</p>');

        } else if (getId == 'italy_txt') {

            $('.customPopDiv .head1').html('<p><strong>Italy:</strong> 60.4 million; Parliamentary republic</p>');

        } else if (getId == 'latvia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Latvia:</strong> 2.3 million; Parliamentary republic</p>');

        } else if (getId == 'liechtenstein_txt') {

            $('.customPopDiv .head1').html('<p><strong>Liechtenstein:</strong> 35,000; Constitutional monarchy</p>');

        } else if (getId == 'lithuania_txt') {

            $('.customPopDiv .head1').html('<p><strong>Lithuania:</strong> 3.4 million; Parliamentary republic</p>');

        } else if (getId == 'luxembourg_txt') {

            $('.customPopDiv .head1').html('<p><strong>Luxembourg:</strong> 0.472 million; Constitutional monarchy</p>');

        } else if (getId == 'macedonia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Macedonia:</strong> 2 million; Parliamentary republic </p>');

        } else if (getId == 'malta_txt') {

            $('.customPopDiv .head1').html('<p><strong>Malta:</strong> 0.408 million; Parliamentary republic</p>');

        } else if (getId == 'moldova_txt') {

            $('.customPopDiv .head1').html('<p><strong>Moldova:</strong> 3.8 million; Parliamentary republic</p>');

        } else if (getId == 'montenegro_txt') {

            $('.customPopDiv .head1').html('<p><strong>Montenegro:</strong> 0.67 million; Parliamentary republic</p>');

        } else if (getId == 'netherland_txt') {

            $('.customPopDiv .head1').html('<p><strong>Netherlands:</strong> 16.7 million; Constitutional monarchy</p>');

        } else if (getId == 'norway_txt') {

            $('.customPopDiv .head1').html('<p><strong>Norway:</strong> 4.9 million; Constitutional monarchy</p>');

        } else if (getId == 'poland_txt') {

            $('.customPopDiv .head1').html('<p><strong>Poland:</strong> 38.1 million; Parliamentary republic</p>');

        } else if (getId == 'portugal_txt') {

            $('.customPopDiv .head1').html('<p><strong>Portugal:</strong> 10.6 million; Parliamentary republic</p>');

        } else if (getId == 'romania_txt') {

            $('.customPopDiv .head1').html('<p><strong>Romania:</strong> 19 million; Republic</p>');

        } else if (getId == 'russia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Russia:</strong> 143 million; Federation/Presidential republic</p>');

        } else if (getId == 'serbia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Serbia:</strong> 7.3 million; Parliamentary republic</p>');

        } else if (getId == 'slovakia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Slovakia:</strong> 5.4 million; Parliamentary republic</p>');

        } else if (getId == 'slovenia_txt') {

            $('.customPopDiv .head1').html('<p><strong>Slovenia:</strong> 2 million; Parliamentary republic</p>');

        } else if (getId == 'spain_txt') {

            $('.customPopDiv .head1').html('<p><strong>Spain:</strong> 47.1 million; Constitutional monarchy</p>');

        } else if (getId == 'sweden_txt') {

            $('.customPopDiv .head1').html('<p><strong>Sweden:</strong> 9.3 million; Constitutional monarchy</p>');

        } else if (getId == 'switz_txt') {

            $('.customPopDiv .head1').html('<p><strong>Switzerland:</strong> 7.7 million; Parliamentary republic</p>');

        } else if (getId == 'ukraine_txt') {

            $('.customPopDiv .head1').html('<p><strong>Ukraine:</strong> 45.9 million; Presidential republic</p>');

        } else if (getId == 'united_kingdom_txt') {

            $('.customPopDiv .head1').html('<p><strong>United Kingdom:</strong> 62 million; Constitutional monarchy</p>');

        }
        $('.customPopDiv').show();
    });

    $('.close1').unbind('click').bind('click', function () {
        $('.customPopDiv').hide();
        count = 0;
        $('.labels li').css('cursor', 'pointer');
    });

});

