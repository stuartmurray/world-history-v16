// ----------------------------------------------------------------------
// -- COURSE SPECIFI JS:
// ----------------------------------------------------------------------
// -- NOTE: This is where you can add anything you need to do specifically to the course, it will load lastly.
// -- ABOUT: THis file will over-ride everything else, if you need to customize
// -- AUTHOR: You - WDS
// ======================================================================

// From dreamweaver template, before conversion to FW 4.0
 
 //    hs.graphicsDir = '../style/graphics/';
 //    hs.outlineType = 'rounded-white';



$(document).ready(function() {

$('.btn').mouseover(function(){
var title = $(this).attr("title");
$(this).click(function(){
self.location.href = title;
});
$(this).removeAttr("title");
$(this).mouseout(function(){
$(this).attr("title", title);
});
});

});


<!--
function showhide(id){ 
if (document.getElementById){ 
obj = document.getElementById(id); 
if (obj.style.display == "none"){ 
obj.style.display = ""; 
} else { 
obj.style.display = "none"; 
} 
} 
} 
//-->

