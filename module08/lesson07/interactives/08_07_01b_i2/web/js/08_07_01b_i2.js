﻿// JScript File

cBInteractive.MultiChoice.Root=null;
cBInteractive.MultiChoice.UserAttempt=0;

cBInteractive.MultiChoice.Init = function(){
	
		
    cBInteractive.MultiChoice.Root = $(".MultiChoice");
    $(cBInteractive.MultiChoice.Root).find(".MCSet:first").show();
    
    cBInteractive.MultiChoice.SetPager(); 

    $(cBInteractive.MultiChoice.Root).find("input").click(function(){
        $(this).closest(".MCSet").find("input").removeClass("attended");
        $(this).addClass("attended");
        $(cBInteractive.MultiChoice.Root).find(".MCSubmit").removeClass("MCDisable").attr({'title':'Submit'});
    });
     
    $(cBInteractive.MultiChoice.Root).find(".MCTryAgain").click(function(){
        var oCurrSet;
        oCurrSet=$(this).closest(".MCSet");
        $(oCurrSet).find("input").removeAttr("checked");
        $(oCurrSet).find(".MCOverlay").hide();
        $(oCurrSet).find(".MCFeedback").hide();
        $(oCurrSet).find(".MCFeedback .MCFeedtxt span").hide();
        cBInteractive.MultiChoice.UserAttempt+=1;
    });
    $(cBInteractive.MultiChoice.Root).find(".MCSubmit").click(function(){
        if(!$(this).hasClass("MCDisable")){
            var oCurrSet;
            
            cBInteractive.MultiChoice.UserAttempt+=1;
            oCurrSet = $(cBInteractive.MultiChoice.Root).find(".MCSet:visible");
			$(oCurrSet).attr('attend','yes');		
            $(this).addClass("MCDisable").removeAttr('title');
            $(oCurrSet).find(".MCFeedback").show();
            $(oCurrSet).find(".MCOverlay").show();
            
            if($(oCurrSet).find("input.attended").hasClass("MCcrct")){
                $(oCurrSet).find(".MCFeedback").find(".MCC").show();
                $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").hide();
                $(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCCorrect");
                if($(cBInteractive.MultiChoice.Root).find(".MCSet:visible").next(".MCSet").length==0){
                    $(cBInteractive.MultiChoice.Root).find(".MCRestart").show();
					$(".MCReverse, .MCForward").hide();
                    $(this).hide();
                }
                else{
                    $(cBInteractive.MultiChoice.Root).find(".MCForward").show();
                }
            }
            else{
                $(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCinCorrect");
                if(cBInteractive.MultiChoice.UserAttempt==1){
                    $(oCurrSet).find(".MCFeedback").find(".MCINC1").show();
                    $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").show();
                }
                else{
                    $(oCurrSet).find(".MCFeedback").find(".MCINC2").show();
                    $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").hide();
                    $(oCurrSet).find("input.MCcrct").parent().find(".MCFeedImg").addClass("MCCorrect");
                    if($(cBInteractive.MultiChoice.Root).find(".MCSet:visible").next(".MCSet").length==0){
                        $(cBInteractive.MultiChoice.Root).find(".MCRestart").show();
						$(".MCReverse, .MCForward").hide();
                        $(this).hide();
                    }
                    else{
                        $(cBInteractive.MultiChoice.Root).find(".MCForward").show();
                    }
                }
            
				
				/*
                $(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCinCorrect");
				$(oCurrSet).find(".MCFeedback").find(".MCINC1").show();
				$(oCurrSet).find(".MCFeedback").find(".MCTryAgain").show();
            */}
        }
    });
    $(cBInteractive.MultiChoice.Root).find(".MCForward").click(function(){
        var oCurrSet;
        $(".MCReverse").show();
        cBInteractive.MultiChoice.UserAttempt=0;
		oCurrSet = $(cBInteractive.MultiChoice.Root).find(".MCSet:visible");
		
		/*$(oCurrSet).next('.MCSet').find('.MCFeedback').css('display') == "block" ||*/
		if($(oCurrSet).next('.MCSet').attr('attend') == "yes"){
			$(this).show();
		}
		else if($(oCurrSet).next('.MCSet').hasClass('lastQues')){
			$(this).hide(); 
			//alert('last');
		}
		else 
		{ 
			$(this).hide(); 
		}
		$(oCurrSet).next(".MCSet").show();
        $(oCurrSet).hide();
        cBInteractive.MultiChoice.SetPager();
    }); 
$(cBInteractive.MultiChoice.Root).find(".MCReverse").click(function(){
		
        oCurrSet = $(cBInteractive.MultiChoice.Root).find(".MCSet:visible");
		if($(oCurrSet).prev(".MCSet").hasClass('firstQues')){
			$(this).hide();
		}

        $(oCurrSet).prev(".MCSet").show();
        $(oCurrSet).hide();
        cBInteractive.MultiChoice.SetPager();
		$(".MCForward").show();
	});
/*    $(cBInteractive.MultiChoice.Root).find(".MCForward").click(function(){
        var oCurrSet;
        
        cBInteractive.MultiChoice.UserAttempt=0;
        $(this).hide();
        oCurrSet = $(cBInteractive.MultiChoice.Root).find(".MCSet:visible");
        $(oCurrSet).next(".MCSet").show();
        $(oCurrSet).hide();
        cBInteractive.MultiChoice.SetPager();
    }); 
*/     
    $(cBInteractive.MultiChoice.Root).find(".MCRestart").click(function(){
		$(".MCReverse, .MCForward").hide();	
        cBInteractive.MultiChoice.UserAttempt=0;
        $(this).hide();
		$('.MCSet').attr('attend','no');
        $(cBInteractive.MultiChoice.Root).find(".MCSet:visible").hide();
        $(cBInteractive.MultiChoice.Root).find(".MCSet:first").show();
        $(cBInteractive.MultiChoice.Root).find(".MCFeedback").hide()
        $(cBInteractive.MultiChoice.Root).find(".MCOverlay").hide();
        $(cBInteractive.MultiChoice.Root).find(".MCFeedback").find("span").hide()
        $(cBInteractive.MultiChoice.Root).find(".MCCorrect, .MCinCorrect").removeClass("MCCorrect").removeClass("MCinCorrect");
        $(cBInteractive.MultiChoice.Root).find("input").removeAttr("checked");
        $(cBInteractive.MultiChoice.Root).find(".MCSubmit").show();
        cBInteractive.MultiChoice.SetPager();
    });
};

cBInteractive.MultiChoice.SetPager=function(){
    $(cBInteractive.MultiChoice.Root).find(".MCPager").text( ($(cBInteractive.MultiChoice.Root).find(".MCSet").index($(cBInteractive.MultiChoice.Root).find(".MCSet:visible"))+1) + "/" + $(cBInteractive.MultiChoice.Root).find(".MCSet").length);
};