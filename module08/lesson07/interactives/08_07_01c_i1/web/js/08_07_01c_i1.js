﻿// JScript File

cBInteractive.MultiChoice.Root=null;
cBInteractive.MultiChoice.UserAttempt=0;
var puzzle_imgIndex = 0;
cBInteractive.MultiChoice.Init = function(){
    cBInteractive.MultiChoice.Root = $(".MultiChoice");
    $(cBInteractive.MultiChoice.Root).find(".MCSet:first").show();
    
    cBInteractive.MultiChoice.SetPager(); 

    $(cBInteractive.MultiChoice.Root).find("input").click(function(){
        $(this).closest(".MCSet").find("input").removeClass("attended");
        $(this).addClass("attended");
        $(cBInteractive.MultiChoice.Root).find(".MCSubmit").removeClass("MCDisable");
    });
     
    $(cBInteractive.MultiChoice.Root).find(".MCTryAgain").click(function(){
        var oCurrSet;
        oCurrSet=$(this).closest(".MCSet");
        $(oCurrSet).find("input").removeAttr("checked");
        $(oCurrSet).find(".MCOverlay").hide();
        $(oCurrSet).find(".MCFeedback").hide();
        $(oCurrSet).find(".MCFeedback .MCFeedtxt span").hide();
        cBInteractive.MultiChoice.UserAttempt+=1;
    });
    $(cBInteractive.MultiChoice.Root).find(".MCSubmit").click(function(){
        if(!$(this).hasClass("MCDisable")){
            var oCurrSet;
            
            cBInteractive.MultiChoice.UserAttempt+=1;
            oCurrSet = $(cBInteractive.MultiChoice.Root).find(".MCSet:visible");
            $(this).addClass("MCDisable");
            $(oCurrSet).find(".MCFeedback").show();
            $(oCurrSet).find(".MCOverlay").show();
            
            if($(oCurrSet).find("input.attended").hasClass("MCcrct")){
                $(oCurrSet).find(".MCFeedback").find(".MCC").show();
                $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").hide();
                $(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCCorrect");
                if($(cBInteractive.MultiChoice.Root).find(".MCSet:visible").next(".MCSet").length==0){
                    $(cBInteractive.MultiChoice.Root).find(".MCRestart").show();
                    $(this).hide();
					showCaption();
                }
                else{
                    $(cBInteractive.MultiChoice.Root).find(".MCForward").show();
                }
				showPuzzleImg();
            }
            else{
                $(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCinCorrect");
                if(cBInteractive.MultiChoice.UserAttempt==1){
/*                    $(oCurrSet).find(".MCFeedback").find(".MCINC1").show();
                    $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").show();
                }
                else{
*/                    $(oCurrSet).find(".MCFeedback").find(".MCINC1").show();
                    $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").hide();
                    $(oCurrSet).find("input.MCcrct").parent().find(".MCFeedImg").addClass("MCCorrect");
                    if($(cBInteractive.MultiChoice.Root).find(".MCSet:visible").next(".MCSet").length==0){
                        $(cBInteractive.MultiChoice.Root).find(".MCRestart").show();
                        $(this).hide();
						showCaption();
                    }
                    else{
                        $(cBInteractive.MultiChoice.Root).find(".MCForward").show();
                    }
					showPuzzleImg();
                }
            }
        }
    });
     
   $(cBInteractive.MultiChoice.Root).find(".MCForward").click(function(){
        var oCurrSet;
        $(".MCReverse").show();
        cBInteractive.MultiChoice.UserAttempt=0;
		oCurrSet = $(cBInteractive.MultiChoice.Root).find(".MCSet:visible");
		
		if($(oCurrSet).next('.MCSet').find('.MCFeedback').css('display') == "block" || $(oCurrSet).next('.MCSet').find('.MCFeedback').css('display') == "inline"){
			$(this).show();
		}else{ 
			$(this).hide(); 
		}
		$(oCurrSet).next(".MCSet").show();
        $(oCurrSet).hide();
        cBInteractive.MultiChoice.SetPager();
    }); 
$(cBInteractive.MultiChoice.Root).find(".MCReverse").click(function(){
		
        oCurrSet = $(cBInteractive.MultiChoice.Root).find(".MCSet:visible");
		if($(oCurrSet).prev(".MCSet").hasClass('firstQues')){
			$(this).hide();
		}

        $(oCurrSet).prev(".MCSet").show();
        $(oCurrSet).hide();
        cBInteractive.MultiChoice.SetPager();
		$(".MCForward").show();
	});
    $(cBInteractive.MultiChoice.Root).find(".MCRestart").click(function(){
		$(".MCReverse").hide();	
		cBInteractive.MultiChoice.UserAttempt=0;
        $(this).hide();
        $(cBInteractive.MultiChoice.Root).find(".MCSet:visible").hide();
        $(cBInteractive.MultiChoice.Root).find(".MCSet:first").show();
        $(cBInteractive.MultiChoice.Root).find(".MCFeedback").hide()
        $(cBInteractive.MultiChoice.Root).find(".MCOverlay").hide();
        $(cBInteractive.MultiChoice.Root).find(".MCFeedback").find("span").hide()
        $(cBInteractive.MultiChoice.Root).find(".MCCorrect, .MCinCorrect").removeClass("MCCorrect").removeClass("MCinCorrect");
        $(cBInteractive.MultiChoice.Root).find("input").removeAttr("checked");
        $(cBInteractive.MultiChoice.Root).find(".MCSubmit").show();
        cBInteractive.MultiChoice.SetPager();
		hidePuzzleImg();
    });
};

cBInteractive.MultiChoice.SetPager=function(){
    $(cBInteractive.MultiChoice.Root).find(".MCPager").text( ($(cBInteractive.MultiChoice.Root).find(".MCSet").index($(cBInteractive.MultiChoice.Root).find(".MCSet:visible"))+1) + "/" + $(cBInteractive.MultiChoice.Root).find(".MCSet").length);
};

function showPuzzleImg(){
	puzzle_imgIndex = $(cBInteractive.MultiChoice.Root).find(".MCSet").index($(cBInteractive.MultiChoice.Root).find(".MCSet:visible"));
	$('.img_txt').hide();
	$('.puzzle_imgCont img:eq('+puzzle_imgIndex+')').show();
}

function hidePuzzleImg(){
	$('.puzzle_imgCont img,.puzzle_caption').hide();
	$('.img_txt').show();
}

function showCaption(){
	$('.puzzle_caption').show();
}