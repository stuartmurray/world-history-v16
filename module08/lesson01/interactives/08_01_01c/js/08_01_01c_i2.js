﻿// JScript File
var dragRevert = true;
var dragCount = 0;


Init = function () {

    InitDrag();

    function InitDrag() {
        $(".ui-draggable").draggable({ containment: '.contentMiddleBg_1', stack: ".dragItem", revert: function () { if (dragRevert == true) { return true; } else { dragRevert = true; } } });
        $('#drag1').show();
        $('.cross-bot-btn').addClass('hidee').removeAttr('title');
    }

    $(".dropItem").each(function () {
        $(this).droppable({
            drop: function (event, ui) {
                var DragEleid = ui.draggable.attr("id");
                var Dragid = ui.draggable.attr("dragid");
                var Dropid = $(this).attr("dropid");

                if (Dragid == Dropid) {
                    //$('#'+DragEleid).css({'left':'0px','top':'0px','zIndex':'0','position':'relative','cursor':'auto','background':'url(../module08/08_01_01c_i2/web/imgs/right.png) no-repeat right #99FFCC'});
                    $('#' + DragEleid).find('.author').show();
                    var htmlTmp = '<div>' + $('#' + DragEleid).html() + '</div>';
                    $(this).append(htmlTmp);
                    //$(this).css({'background':'url(../module08/08_01_01c_i2/web/imgs/right.png) no-repeat right #99FFCC'});
                    $('#' + DragEleid).hide();
                    dragCount = dragCount + 1;
                    if (dragCount < 7) {
                        showNextDrag(dragCount);
                    } else {
                        enableRestart();
                    }
                    //showFeed();
                }
            }
        });
    });

    if (/iphone|ipod|ipad|android/i.test(navigator.userAgent)) {
        init();
    }

    function enableRestart() {
        $('.cross-bot-btn').removeClass('hidee').attr('title', 'Restart');
    }

    $('.cross-bot-btn').click(function () {
        if (!$('.cross-bot-btn').hasClass('hidee'))
            $('.dropItem').html('');
        InitDrag();
        dragCount = 0;
        $('.author').hide();
    });


    function showNextDrag(index) {
        $('.dragItem:eq(' + index + ')').show();
    }

    function showFeed() {
        if ($('.dragarea .dragItem').length == 0) {
            $('.finalFB').show();
        } else {
            $('.corctFB').show();
        }
        $('.overlay, .feed_div').show();
    }

    $('.feed_close').live('click', function () {
        $('.overlay, .feed_div, .finalFB, .corctFB').hide();
    });

    function init() {
        var a = 0;
        $(".dragItem").each(function () {
            var b = $(this).attr("id");
            var c = document.getElementById(b);
            a++;
            c.addEventListener("touchstart", touchHandler, true);
            c.addEventListener("touchmove", function (e) { e.preventDefault(); }, true);
            c.addEventListener("touchmove", touchHandler, true);
            c.addEventListener("touchend", touchHandler, true);
            c.addEventListener("touchcancel", touchHandler, true);
        });
    }

    function touchHandler(a) {
        var b = a.changedTouches, c = b[0], d = "";
        switch (a.type) {
            case "touchstart":
                d = "mousedown";
                break;
            case "touchmove":
                d = "mousemove";
                break;
            case "touchend":
                d = "mouseup";
                break;
            default:
                return
        }
        var e = document.createEvent("MouseEvent");
        e.initMouseEvent(d, true, true, window, 1, c.screenX, c.screenY, c.clientX, c.clientY, false, false, false, false, 0, null);
        c.target.dispatchEvent(e);
    }

};

$(function () {
    Init();
});