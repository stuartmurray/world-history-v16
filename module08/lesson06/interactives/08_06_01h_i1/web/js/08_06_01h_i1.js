﻿// JScript File
var enaSubmit = false;
cBInteractive.MultiChoice.Init = function(){
var arr_grp=[[],
['textbox5','textbox6','textbox7'],
['textbox10','textbox11','textbox12','textbox13','textbox14'],
['textbox17','textbox18','textbox19','textbox20','textbox21','textbox22','textbox23','textbox24','textbox25','textbox26','textbox27'],
['textbox32','textbox33','textbox34','textbox35','textbox36','textbox37','textbox38','textbox39','textbox40','textbox41','textbox42','textbox100'],
['textbox64','textbox65','textbox66','textbox67','textbox68','textbox69','textbox70','textbox71','textbox72','textbox101'],
['textbox75','textbox76','textbox77'],
['textbox83','textbox84','textbox85','textbox86','textbox87','textbox88','textbox89','textbox90','textbox91','textbox92'],
['textbox96','textbox97','textbox98'],
[],
["textbox1","textbox2","textbox4","textbox7"],
["textbox3","textbox5","textbox8","textbox15","textbox29","textbox34","textbox44","textbox50","textbox56","textbox61"],
["textbox9","textbox18","textbox31","textbox100","textbox48","textbox53","textbox58","textbox62","textbox67","textbox73","textbox77","textbox79","textbox81","textbox92","textbox94","textbox98","textbox99"],
["textbox14","textbox28","textbox32","textbox43","textbox49","textbox55","textbox60"],
["textbox16","textbox30","textbox37","textbox45","textbox51"],
["textbox39","textbox46"],
["textbox41","textbox47","textbox52","textbox57"],
["textbox54","textbox59","textbox63","textbox70","textbox74","textbox78","textbox80","textbox82","textbox93","textbox95","textbox102"]
]


var ans_arr=["","EEC","NAFTA","OUTSOURCING","GROUPOFEIGHT","PIED-NOIRS","IMF","METROPOLES","WTO","","ECSC","DEVELOPING","CULTURALDIFFUSION","ALGERIA","KYOTO","EU","GATT","DEMOGRAPHIC"];

var bot_text=["","cross-bot-textbox5","cross-bot-textbox4","cross-bot-textbox3","cross-bot-textbox11","cross-bot-textbox2","cross-bot-textbox7","","cross-bot-textbox10","cross-bot-textbox9","cross-bot-textbox1","cross-bot-textbox6","cross-bot-textbox8"];

var selected_arr=null;
var ptr=0;
var attempt=0;
var n=0;

var completecount=0;
//var checkans=0;
$('.crosswordleft input').attr('disabled','disabled');
$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
$('.cross-bot-btn').addClass('hidee').removeAttr('title');
$('.crosswordleft input').addClass('textbox'); // To make inputbox disabled when popup is open
function initcross(){
 selected_arr=null;
 ptr=0;
 attempt=0;
 n=0;
 completecount=0
$('.crosswordleft input').attr('disabled','disabled').removeAttr('readonly');
$('.cross-bot-btn').addClass('hidee').removeAttr('title');
$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
$('.done').removeAttr('style');
$('.done').removeClass('done');
$('.active').removeClass('active')
$('.lockked').removeClass('lockked').removeClass('bggreen');
$('.crosstxtarea').css('cursor','pointer');
for(var i=0;i<arr_grp.length;i++){
	for(var j=0;j<arr_grp[i].length;j++){
		$('.'+arr_grp[i][j]).val('');
	}
}


for(i=0;i<bot_text.length;i++){
	if(bot_text[i]!="")
		$('.'+bot_text[i]).val('');	
	}
	
}

$('.crosstxtarea').hover(function(){
		if($(this).css('cursor') == "pointer")						  
		$(this).css({"background":"#EAF7FF"});						  
	},function(){
		if($(this).css('cursor') == "pointer")			
		$(this).css({"background":"#d6eefe"});			
});

$('.crosstxtarea').click(function(){
	if($(this).css('cursor') == "pointer"){	
		var numVal = $(this).find('.no').html();
		$('.cross_numbers span').show();
		$('.num_'+numVal+'').hide();
		enaSubmit = false;
		n=$(this).index();	
		selected_arr=arr_grp[n];
	if(document.onkeypress==null)
		document.onkeypress=keyinput
		if(!($(this).hasClass('done') || $(this).hasClass('active'))){
			enaSubmit = true;
		disableOtherInputs();
		focusInput();
		
		n=$(this).index();	
		selected_arr=arr_grp[n];
		
		attempt=0;
		$('.active').removeClass('active');
		$(this).addClass('active');
		if(isFilled() && $('.popup-bg').css('display') == "none" && enaSubmit){		
			$('.cross-check').removeClass('disabledd').css({'cursor':'pointer'}).attr('title','Check');
		}else{		
			$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
		}
		}
	}
});

function disableOtherInputs(){
	$('.crosswordleft input:not(".bggreen")').attr('disabled','disabled');
	for(i = 0; i < selected_arr.length; i++){
		//if(!($('.'+selected_arr[i]).hasClass('bggreen'))){
		$('.'+selected_arr[i]).removeAttr('disabled');
		//}
	}
}

function focusInput(){
	var cnt = 0;
	while(cnt < selected_arr.length){
		if(!($('.'+selected_arr[cnt]).hasClass('lockked'))){
			$('.'+selected_arr[cnt]).focus();// To trigger keypad in ipad
			ptr=cnt;
			break;
		}else{
			cnt++;	
		}
	}
}

$('.crosswordleft input').focus(function(){
	if($(this).hasClass('bggreen')){
		$(this).blur();
	}
	var classNam  = $(this).attr('class');
	classNam = classNam.replace(" textbox","");
	var classInd = $.inArray(classNam, selected_arr);
	ptr = classInd;
	document.onkeypress=keyinput;
});

function keyinput(e){
var evtobj=window.event? event : e //distinguish between IE's explicit event object (window.event) and Firefox's implicit.
var unicode=evtobj.charCode? evtobj.charCode : evtobj.keyCode

var actualkey=String.fromCharCode(unicode);
	if(unicode == 9){
		if(ptr == selected_arr.length-1){
			ptr=0;
			givefirstFocus();
			return false;
		}
	}
	if((unicode >= 65 && unicode <= 90) || (unicode >= 97 && unicode <= 122) || (unicode == 9)|| (unicode == 45))
	{}
	else{
		$('.popup-bg, .popup-innerbg-number, .overlay').show();	
		return false;
	}
	if(actualkey>='a' && actualkey <='z'){
	  actualkey=actualkey.toUpperCase();
	 }
	if(actualkey>='A' && actualkey <='Z' &&(selected_arr != null) || actualkey =='-'){
		if(!$('.'+selected_arr[ptr]).hasClass('lockked')){
			$('.'+selected_arr[ptr]).val(actualkey);
			//alert(selected_arr.length +'---'+ ptr);
			if(ptr != selected_arr.length-1){
				if($('.'+selected_arr[ptr+1]).hasClass('lockked') == true){
					$('.'+selected_arr[ptr+2]).focus();
				}else{
					$('.'+selected_arr[ptr+1]).focus();
				}
			}else{
				ptr=0;
				givefirstFocus();
			}
		}
		
		if(isFilled() && $('.popup-bg').css('display') == "none" && enaSubmit){	
		$('.cross-check').removeClass('disabledd').css({'cursor':'pointer'}).attr('title','Check');
		}
	
	}
}

function givefirstFocus(){
	while(ptr < selected_arr.length){
		if(!$('.'+selected_arr[ptr]).hasClass('lockked')){
			$('.'+selected_arr[ptr]).focus();
			break;
		}else{
		ptr++;
		}
	}	
}

$('.cross-check').click(function(){
if(!$('.cross-check').hasClass('disabledd')){
	$('.crosswordleft .textbox').addClass('lockked');	// To make inputbox disabled when popup is open
	attempt++;
	var str="";	
	for(var i=0;i<selected_arr.length;i++)
	{
		str=str+$('.'+selected_arr[i]).val();
	}
	if(str == ans_arr[n]){
		$('.active').css('opacity','0.5')
		$('.active').css('cursor','default');
		$('.active').addClass('done');
		var cluenum = $('.active').attr('clueno'); // to turn the clues red
		$('.clues li').eq(cluenum).css('color','red')

		$('.done').removeClass('active')
		$('.popup-bg').show();
		$('.popup-innerbg-correct').show();	
		$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
		completecount++;
		attempt=0;
		setAllLocked();
		if(completecount>=16){
		$('.cross-bot-btn').removeClass('hidee').attr('title','Restart');
		}
		else if(completecount>=7){
		$('.showentireans').show();
		$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
		}
	}else{
		if(attempt>=2){
			$('.popup-bg').show();
			$('.popup-innerbg-showans').show();
			$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
		}else{
			$('.popup-bg').show();
			$('.popup-innerbg-try').show();			
			$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
			}
	}
	}
})
function isFilled(){
for(var i=0;i<selected_arr.length;i++)
	{
		if($('.'+selected_arr[i]).val()=="")
		return false;
	}
	return true;
}


$('.close-small-bt').click(function(){
	$('.crosswordleft .textbox').removeClass('lockked');		// To make inputbox disabled when popup is open					
	$(this).parent().parent().hide();	
	$(this).parent().parent().parent().hide();
	$('.popup-bg, .pop_over, .overlay').hide();
	for(var i=0;i<selected_arr.length;i++){
		$('.'+selected_arr[i]).addClass('lockked').removeClass('textbox');	
	}	
	selected_arr = null;
});

$('.close_num').click(function(){
	$('.popup-bg, .popup-innerbg-number, .overlay').hide();	
});

$('.tryagain-bt').click(function(){
	$('.crosswordleft .textbox').removeClass('lockked');
	$('.popup-bg').hide();
	$('.popup-innerbg-try').hide();
	focusInput();// To trigger keypad in ipad	
});

$('.reset').click(function(){
	$('.crosswordleft .textbox').removeClass('lockked');
	$('.popup-bg').hide();
	$('.popup-innerbg-try').hide();
	for(var i=0;i<selected_arr.length;i++){
		$('.'+selected_arr[i]).val('');	
	}	
	focusInput();// To trigger keypad in ipad	
});

$('.showans-bt').click(function(){
	$('.cross_numbers span').show();							
	$('.crosswordleft .textbox').removeClass('lockked');	// To make inputbox disabled when popup is open				
	$('.popup-bg').hide();
	$('.popup-innerbg-showans').hide();
	$('.active').css('opacity','0.5')
		$('.active').css('cursor','default');
		$('.active').addClass('done');
		var cluenum = $('.active').attr('clueno'); // to turn the clues red
		$('.clues li').eq(cluenum).css('color','red')
		$('.done').removeClass('active')
	for(var i=0;i<selected_arr.length;i++)
	{
		$('.'+selected_arr[i]).val(ans_arr[n].substr(i,1));	
		$('.'+selected_arr[i]).addClass('lockked').addClass('bggreen').attr('readonly','readonly').removeClass('textbox');
		}	
	
	$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
	completecount++;
	if(completecount>=16){
		$('.cross-bot-btn').removeClass('hidee').attr('title','Restart');
	}else if(completecount>=7){
		$('.showentireans').show();
		$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
	}
	selected_arr = null;	
})
$('.cross-bot-btn').click(function(){
	if(!$('.cross-bot-btn').hasClass('hidee'))
		$('.clues li').css('color','#444444');
		$('.crosswordleft input').addClass('textbox');
		initcross();
});
function setAllLocked(){
for(var i=0;i<selected_arr.length;i++)
	{
	$('.'+selected_arr[i]).addClass('lockked').addClass('bggreen').attr('readonly','readonly');		
	}	
}

$('#hint').live('click', function(){
	$('#content_sction, .overlay').show();							  
								  
});

$('.hint_close').live('click', function(){
	$('#content_sction, .overlay').hide();							  
});

$('.showentireans').live('click', function(){
	$(this).hide();		
	$('.cross-bot-btn').removeClass('hidee').attr('title','Restart');
	$('.clues li').css('color','red');
	$('.crosstxtarea').each(function(){
		n=$(this).index();	
		selected_arr=arr_grp[n];
		for(var i=0;i<selected_arr.length;i++){
			$('.'+selected_arr[i]).val(ans_arr[n].substr(i,1));	
			$('.'+selected_arr[i]).addClass('lockked').removeAttr('disabled').attr('readonly','readonly');
		}							  
	});
	$('.crosstxtarea').css('cursor','default');
});


};

