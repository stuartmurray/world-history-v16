﻿cBInteractive.ClickRevealStar.Init = function(){
	var term_index = -1;
	var desc_index = -1;
	var termObj;
	var descObj;
	$('.term_ul li,.desc_ul li').addClass('list');
	
	$('.term_ul li').live('click',function(){
		if(!$(this).hasClass('correct')){
			if($('.term_ul li').hasClass('list')){
				resetBgColor_1($('.term_ul .list'));
			}
			term_index = $(this).index();
			termObj = $(this);
			setBgColor($(this));
			showFeedback();
		}
	});
	
	$('.desc_ul li').live('click',function(){
		if(!$(this).hasClass('correct')){
			if($('.desc_ul li').hasClass('list')){
				resetBgColor_1($('.desc_ul .list'));
			}
			desc_index = $(this).attr('index');
			descObj = $(this);
			setBgColor($(this));
			showFeedback();
		}
	});
	
	function setBgColor(obj){
		obj.css({'background':'#ee5858'});
	}
	
	function resetBgColor(obj1,obj2){
		obj1.css({'background':'#ffffff'});
		obj2.css({'background':'#ffffff'});
	}
	
	function resetBgColor_1(obj){
		obj.css({'background':'#ffffff'});
	}
	
	function showFeedback(){
		if(term_index != -1 && desc_index != -1){
			if(term_index == desc_index){
				showPositiveFeedback();
			}else{
				showTryAgain();
			}
		}
	}
	
	function showPositiveFeedback(){
		$(".goodjob,.overlay").show();
	}
	
	function showTryAgain(){
		$('.tryAgain,.overlay').show();
	}
	
	$('.tryAgainBtn').live('click',function(){
		resetIndex();
		$('.tryAgain,.overlay').hide();
		resetBgColor(termObj,descObj);
	});
	
	function resetIndex(){
		term_index = -1;
		desc_index = -1;
	}
	
	$('.close').live('click',function(){
		resetIndex();
		$('.goodjob,.overlay').hide();
		resetBorder(termObj,descObj);
	});
	
	function resetBorder(obj1,obj2){
		var ind = obj2.attr('index');
		var li_ind = obj2.index();
		
		var ind_html = $('.desc_ul li:eq('+ind+')').html();
		var old_ind = $('.desc_ul li:eq('+ind+')').attr('index');
		var correct_html = obj2.html();
		
		$('.desc_ul li:eq('+li_ind+')').html(ind_html).attr('index',old_ind);
		$('.desc_ul li:eq('+ind+')').html(correct_html).attr('index',ind);
		
		if(ind%2 == 0){
			var bg_color = '#79f079';
			var bg_border = '2px solid #79f079';
		}else{
			var bg_color = '#22dbf1';
			var bg_border = '2px solid #22dbf1';
		}
		
		obj1.css({'background':bg_color,'border': bg_border,'cursor':'default'});
		$('.desc_ul li:eq('+ind+')').css({'background':bg_color,'border':bg_border,'cursor':'default'});
		obj1.addClass('correct').removeClass('list');
		$('.desc_ul li:eq('+ind+')').addClass('correct').removeClass('list');
		
		if(ind != li_ind){
			resetBgColor_1(obj2);
		}
	}
}