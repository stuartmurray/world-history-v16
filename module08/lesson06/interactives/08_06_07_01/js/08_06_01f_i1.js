﻿// JScript File
var dragRevert = true;


Init = function () {

    InitDrag();

    function InitDrag() {
        $(".ui-draggable").draggable({ containment: '.contentMiddleBg', stack: ".dragItem", revert: function () { if (dragRevert == true) { return true; } else { dragRevert = true; } } });
    }

    $(".dropItem").each(function () {
        $(this).droppable({
            drop: function (event, ui) {
                var DragEleid = ui.draggable.attr("id");
                var Dragid = ui.draggable.attr("dragid");
                var Dropid = $(this).attr("dropid");
                if (Dragid == Dropid) {
                    $('#' + DragEleid).css({ 'left': '0px', 'top': '0px', 'zIndex': '0', 'cursor': 'auto', 'background': '#99FFCC', 'padding': '3px' }).draggable('disable');
                    $(this).append(ui.draggable);
                    showFeed();
                    $('.corctFB').show();
                    $('.wrongFB').hide();
                }
                else {
                    showFeed();
                    $('.corctFB').hide();
                    $('.wrongFB').show();

                }
            }
        });
    });

    if (/iphone|ipod|ipad|android/i.test(navigator.userAgent)) {
        init();
    }

    function showFeed() {

        $('.overlay, .feed_div').show();
    }

    $('.feed_close').live('click', function () {
        $('.overlay, .feed_div, .finalFB, .corctFB').hide();
    });

    function init() {
        var a = 0;
        $(".dragItem").each(function () {
            var b = $(this).attr("id");
            var c = document.getElementById(b);
            a++;
            c.addEventListener("touchstart", touchHandler, true);
            c.addEventListener("touchmove", function (e) { e.preventDefault(); }, true);
            c.addEventListener("touchmove", touchHandler, true);
            c.addEventListener("touchend", touchHandler, true);
            c.addEventListener("touchcancel", touchHandler, true);
        });
    }

    function touchHandler(a) {
        var b = a.changedTouches, c = b[0], d = "";
        switch (a.type) {
            case "touchstart":
                d = "mousedown";
                break;
            case "touchmove":
                d = "mousemove";
                break;
            case "touchend":
                d = "mouseup";
                break;
            default:
                return
        }
        var e = document.createEvent("MouseEvent");
        e.initMouseEvent(d, true, true, window, 1, c.screenX, c.screenY, c.clientX, c.clientY, false, false, false, false, 0, null);
        c.target.dispatchEvent(e);
    }

};

$(function () {
    Init();
});