﻿var count = 0;
$(document).ready(function () {
    setTimeout(function () {
        $('.hiddenglobal').hide();
        $(".aud").bind('click', function () {
            disableHide1();
            var audid = $(this).attr("id");
            audid = audid.slice(7, 8);
            $('.hiddentool' + audid).show();
        });
        $('.close2').bind('click', function () {
            $('.hiddenglobal').hide();
            disableHide1();
        });
        $('.jcarousel-item').bind('click', function () {
            $('.hiddenglobal').hide();
            $('.mejs-button').each(function () {
                if ($(this).hasClass('mejs-pause')) {
                    $(this).find('button').trigger('click');
                }
            });
        });
    }, 500);
});


function disableHide1() {
    $('.hiddenglobal').hide();
    $('.mejs-button').each(function () {
        if ($(this).hasClass('mejs-pause')) {
            $(this).find('button').trigger('click');
        }
    });
}

