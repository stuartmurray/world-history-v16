﻿$(document).ready(function () {
    $('.divclass').css('display', 'none');
    $('.image-container-normal').unbind('click').bind('click', function () {        
            $('.divclass').css('display', 'block');
    });

    $('.ui-icon-closethick').unbind('click').bind('click', function () {
        $('.divclass').css('display', 'none');
    });
    $('.ui-dialog-buttonset').unbind('click').bind('click', function () {
        $('.divclass').css('display', 'none');
    });
});