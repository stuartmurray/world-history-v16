﻿// JScript File

Init = function () {

    $(".tab").find('.nxt').click(function () {
        $('.page1_c1, .page2_c1, .page2_c2, .page2_c3, .page3_c1, .page4_c1, .page4_c2, .page4_c3').hide();

        $(".prev").css('cursor', 'pointer');
        var oCurrSet = $(this).closest(".tab").find(".set:visible");
        $(oCurrSet).next(".set").show();
        $(oCurrSet).hide();
        $(this).closest(".tab").find(".prev").removeAttr("disabled").css({ 'opacity': '1', 'filter': 'alpha(opacity = 100)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)' }).attr({ 'title': 'Previous' });

        if ($(oCurrSet).next(".set").next(".set").length == 0) {
            $(this).closest(".tab").find(".nxt").attr("disabled", "true").removeAttr('title');
            $(this).closest(".tab").find(".nxt").css({ 'cursor': 'default' }).css({ 'opacity': '0.3', 'filter': 'alpha(opacity = 30)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)' }); ;
        }
        //pageNo = 1;
        //  pageNo++;

        $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) + 1) + "/" + ($(this).closest(".tab").find(".set").length));

    });

    $(".tab").find('.prev').click(function () {
        $('.page1_c1, .page2_c1, .page2_c2, .page2_c3, .page3_c1, .page4_c1, .page4_c2, .page4_c3').hide();
        $(".nxt").css('cursor', 'pointer');

        var oCurrSet = $(this).closest(".tab").find(".set:visible");
        $(this).closest(".tab").find(".nxt").removeAttr("disabled").css({ 'opacity': '1', 'filter': 'alpha(opacity = 100)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)' }).attr({ 'title': 'Next' });
        $(oCurrSet).prev(".set").show();
        $(oCurrSet).hide();
        if ($(oCurrSet).prev(".set").prev(".tab .set").length == 0) {
            $(this).closest(".tab").find(".prev").attr("disabled", "true").removeAttr('title');
            $(this).closest(".tab").find(".prev").css({ 'cursor': 'default' }).css({ 'opacity': '0.3', 'filter': 'alpha(opacity = 30)', '-ms-filter': 'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)' });
        }
        // pageNo--;

        $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) + 1) + "/" + ($(this).closest(".tab").find(".set").length));

    });

    $('a.lightbox').each(function () {
        $(this).lightBox({ fixedNavigation: true });
        $(this).append($(document.createElement("div")).addClass("magGlass"));
        $('.magGlass').css({ left: '274px', top: '270px' });
    });

    $('.clk1_1, .clk1_2, .clk1_3, .pageclk1_1').live('click', function () {

        $('.page1_c1').show();
    });
    $('.clk2_1, .pageclk2_1').live('click', function () {
        $('.page2_c3').show();
        $('.page2_c1, .page2_c2').hide();
    });

    $('.clk2_2, .clk2_3, .clk2_4, .clk2_5, .clk2_6, .clk2_7, .clk2_8, .clk2_9, .clk2_10, .pageclk2_2').live('click', function () {
        $('.page2_c1').show();
        $('.page2_c2, .page2_c3').hide();
    });

    $('.clk2_11, .clk2_12, .clk2_13, .clk2_14, .pageclk2_3').live('click', function () {
        $('.page2_c2').show();
        $('.page2_c1, .page2_c3').hide();
    });
    $('.clk3_1, .clk3_2, .clk3_3, .clk3_4, .clk3_5, .clk3_6, .clk3_7, .clk3_8, .pageclk3_1').live('click', function () {
        $('.page3_c1').show();
    });
    $('.clk4_1, .clk4_2').live('click', function () {
        $('.page4_c1').show();
        $('.page4_c2, .page4_c3').hide();

    });
    $('.clk4_4, .map4c').live('click', function () {
        $('.page4_c3').show();
        $('.page4_c2, .page4_c1').hide();
    });
    $('.clk4_3').live('click', function () {
        $('.page4_c2').show();
        $('.page4_c3, .page4_c1').hide();
    });
}

/*flvs*/
$(function () {
    Init();
});

var check = 0;
$('._audio-tip').click(function () {
    check = 1;
    for (var i = 0; i < $("audio").length; i++) {
        $("audio")[i].pause();
    }
    $('.tooltip').hide();
    $('#tooltip_' + $(this).attr('id')).show();
    //    console.log('show');
});
$(document).click(function (e) {
    if (check == 1) {
        check = 0;
        return;
    }
    var $target = $(e.target);
    if ($target.closest('.tooltip').length == 0) {
        //        console.log('hide');
        for (var i = 0; i < $("audio").length; i++) {
            $("audio")[i].pause();
        }
        $('.tooltip').hide();
    }
});