﻿$(document).ready(function () {
    $('.droppable').on('drop', function () {
            setTimeout(function () {
                $('.img').each(function () {
                    if ($(this).find('.draggable').hasClass('ui-draggable-disabled')) {
                        $(this).find('.copyrightDrag').hide();
                    }
                });
            }, 100);
    });
});