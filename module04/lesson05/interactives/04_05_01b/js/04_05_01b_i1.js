﻿// JScript File
var count = 0;
var nextdisable = true;
var prevdisable = false;
var time;

Init = function(){


$('.slide_next').live('click',function(){
	prevdisable = true;								   
	if(nextdisable){								   
		count++;
		if(count >= 2){
			$('.slide_next').css({'cursor':'auto'}).removeAttr('title').attr('src','imgs/next_d.png');
			nextdisable = false;
		}
		$('.slide_prev').css({'cursor':'pointer'}).attr({'title':'Previous'}).attr('src','imgs/prev.png');
		$(".slide_con1").animate({"marginLeft": "-=580px"}, "slow");
		$('.page_count').html(count+1);
	}
});
 
$('.slide_prev').live('click',function(){  
	nextdisable = true;
	if(prevdisable){								   
		count--;
		if(count <= 0){
			$('.slide_prev').css({'cursor':'auto'}).removeAttr('title').attr('src','imgs/prev_d.png');
			prevdisable = false;
		}
		$('.slide_next').css({'cursor':'pointer'}).attr({'title':'Next'}).attr('src','imgs/next.png');
		$(".slide_con1").animate({"marginLeft": "+=580px"}, "slow");
		$('.page_count').html(count+1);
	}
});


$(".tooltip").click(function(){
	return false;
})

$('.card1, .card2, .card3, .card4, .card5, .card6').live('click', function(){
	//alert($(this).attr('class'));
	var classNam = $(this).attr('class');
	var imgSrc = $('.'+classNam+' .Img1').attr('src');
	if(imgSrc.indexOf('back') == -1){
		time = setTimeout(function(){
			$('.'+classNam+' .img_txt').show();
		},500);
	}else{
		clearTimeout(time);
		$('.'+classNam+' .img_txt').hide();
	}
	$(this).attr('class');
	Flip(this);	
});




function Flip(obj){
	var iWidth;
	iWidth = $(obj).width();
	$(obj).children(".Img1").css("position","absolute");
	$(obj).children(".Img1").css("z-index","1000");
	$(obj).children(".Img1").css("left","0px");
	$(obj).children(".Img2").css("position","absolute");
	$(obj).children(".Img2").css("z-index","999");
	$(obj).children(".Img1").show();
	
	$(obj).children(".Img1").animate( {left: (iWidth/2) + "px", 
	width:"10px"}, 200, "linear", function(){
	$(this).css("z-index","999");
	$(this).hide();
	$(this).parent().children(".Img2").css("left", (iWidth/2) + "px");
	$(this).parent().children(".Img2").css("width","10px");
	$(this).parent().children(".Img2").show();
	
	$(this).parent().children(".Img2").animate( {left:"0px", width: iWidth + "px"}, 200, "linear", function(){
	$(this).css("left","0px");
	$(this).css("width", iWidth + "px");
	$(this).css("z-index","1000");
	
	$(this).parent().children(".Img1").css("left","0px");
	$(this).parent().children(".Img1").css("width", iWidth + "px");
	$(this).parent().children(".Img2").attr("class","Img2001");
	$(this).parent().children(".Img1").attr("class","Img2");
	$(this).parent().children(".Img2001").attr("class","Img1");
	});
	});

}

}
Init();