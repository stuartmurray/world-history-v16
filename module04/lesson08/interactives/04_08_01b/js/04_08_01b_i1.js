﻿// JScript File
var dragRevert = true;
Init = function(){
var arr_grp=[[],
['textbox2','textbox3','textbox4','textbox5','textbox6','textbox7','textbox8','textbox9','textbox10','textbox11'],
['textbox14','textbox15','textbox16','textbox17','textbox18','textbox19','textbox20','textbox21','textbox22','textbox23','textbox24'],
['textbox31','textbox32','textbox33','textbox34','textbox35','textbox36','textbox37'],
['textbox52','textbox53','textbox54','textbox55','textbox56','textbox57','textbox58','textbox59','textbox60','textbox61','textbox62','textbox63','textbox64'],
['textbox65','textbox66','textbox67','textbox68','textbox69','textbox70','textbox71'],
['textbox82','textbox83','textbox84','textbox85','textbox86','textbox87','textbox88','textbox89','textbox90'],
['textbox112','textbox113','textbox114','textbox115','textbox116','textbox117','textbox118'],
[],
["textbox1","textbox8","textbox12","textbox13","textbox22","textbox25","textbox28","textbox38","textbox43","textbox48","textbox60"],
["textbox26","textbox29","textbox40","textbox45","textbox50","textbox64","textbox75","textbox81","textbox93","textbox99","textbox104","textbox108","textbox111","textbox119"],
["textbox27","textbox32","textbox42","textbox47","textbox53","textbox72","textbox77","textbox87","textbox95","textbox100","textbox105"],
["textbox30","textbox41","textbox46","textbox51","textbox70","textbox76","textbox83","textbox94"],
["textbox39","textbox44","textbox49","textbox62","textbox74","textbox80","textbox92","textbox98","textbox103","textbox107","textbox110"],
["textbox58","textbox73","textbox79","textbox91","textbox97","textbox102"],
["textbox78","textbox90","textbox96","textbox101","textbox106","textbox109","textbox117"]
]


var ans_arr=["","ENCOMIENDA","RENAISSANCE","CARAVEL","CONQUISTADORS","PATRONS","ASTROLABE","PENANCE","","RECONQUISTA","PREDESTINATION","NATIONALISM","HUMANISM","REFORMATION","SIMONY","HERETIC"];


var selected_arr=null;
var ptr=0;
var attempt=0;
var n=0;

var completecount=0;
$('.cross-check').addClass('disabledd').css({'cursor':'auto'});
$('.cross-bot-btn').addClass('hidee').removeAttr('title');
function initcross(){
 selected_arr=null;
 ptr=0;
 attempt=0;
 n=0;
 completecount=0
$('.cross-bot-btn').addClass('hidee').removeAttr('title');
$('.cross-check').addClass('disabledd').css({'cursor':'auto'});
$('.done').css('opacity','1')
$('.done').removeClass('done');
$('.active').removeClass('active')
$('.lockked').removeClass('lockked');
$('.crosstxtarea').css('cursor','pointer');
for(var i=0;i<arr_grp.length;i++){
	for(var j=0;j<arr_grp[i].length;j++){
		$('.'+arr_grp[i][j]).val('');
	}
}

}
$('.crosstxtarea').click(function(){
	
	n=$(this).index();	
	selected_arr=arr_grp[n];
	var focusinp = selected_arr[0];// To trigger keypad in ipad
	$('.'+focusinp).focus();								  
if(document.onkeypress==null)
	document.onkeypress=keyinput
	if(!($(this).hasClass('done') || $(this).hasClass('active'))){
	n=$(this).index();	
	selected_arr=arr_grp[n];
	
	ptr=0;attempt=0;
	$('.active').removeClass('active');
	$(this).addClass('active');
	if(isFilled()){		
		$('.cross-check').removeClass('disabledd').css({'cursor':'pointer'}).attr('title','Submit');
	}else{		
		$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
	}
	}
	
})

function keyinput(e){
var evtobj=window.event? event : e //distinguish between IE's explicit event object (window.event) and Firefox's implicit.
var unicode=evtobj.charCode? evtobj.charCode : evtobj.keyCode

var actualkey=String.fromCharCode(unicode);
	if(actualkey>='a' && actualkey <='z')
	  actualkey=actualkey.toUpperCase();
	  
	if(actualkey>='A' && actualkey <='Z'){
		if(!$('.'+selected_arr[ptr]).hasClass('lockked'))
			$('.'+selected_arr[ptr]).val(actualkey);
		ptr++;
		if(ptr>selected_arr.length-1)
		  ptr=0;
	
	if(isFilled()){	
	$('.cross-check').removeClass('disabledd').css({'cursor':'pointer'}).attr('title','Submit');
	}
	
	}
}

$('.cross-check').click(function(){
if(!$('.cross-check').hasClass('disabledd')){
	attempt++;
	var str="";	
	for(var i=0;i<selected_arr.length;i++)
	{
		str=str+$('.'+selected_arr[i]).val();
	}
	if(str == ans_arr[n]){
		$('.active').css('opacity','0.5')
		$('.active').css('cursor','default');
		$('.active').addClass('done');
		
		var cluenum = $('.active').attr('clueno'); // to turn the clues red
		$('.clues li').eq(cluenum).css('color','red')

		$('.done').removeClass('active')
		$('.popup-bg').show();
		$('.popup-innerbg-correct').show();	
		$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
		completecount++;
		attempt=0;
		setAllLocked();
		if(completecount>=14){
		$('.cross-bot-btn').removeClass('hidee').attr('title','Restart');
		}
	}else{
		if(attempt>=2){
			$('.popup-bg').show();
			$('.popup-innerbg-showans').show();
		}else{
			$('.popup-bg').show();
			$('.popup-innerbg-try').show();			
			$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
			}
	}
	}
})
function isFilled(){
for(var i=0;i<selected_arr.length;i++)
	{
		if($('.'+selected_arr[i]).val()=="")
		return false;
	}
	return true;
}


$('.close-small-bt').click(function(){
	$(this).parent().parent().hide();	
	$(this).parent().parent().parent().hide();
});

$('.tryagain-bt').click(function(){
$('.popup-bg, .overlay').hide();
$('.popup-innerbg-try').hide();
})

$('.showans-bt').click(function(){
	$('.popup-bg, .overlay').hide();
	$('.popup-innerbg-showans').hide();
	$('.active').css('opacity','0.5')
		$('.active').css('cursor','default');
		$('.active').addClass('done');
		var cluenum = $('.active').attr('clueno'); // to turn the clues red
		$('.clues li').eq(cluenum).css('color','red')
		
		$('.done').removeClass('active')
	for(var i=0;i<selected_arr.length;i++)
	{
		$('.'+selected_arr[i]).val(ans_arr[n].substr(i,1));	
		$('.'+selected_arr[i]).addClass('lockked');	
		}	
	
	$('.cross-check').addClass('disabledd').css({'cursor':'auto'}).removeAttr('title');
	completecount++;
	if(completecount>=14)
		$('.cross-bot-btn').removeClass('hidee').attr('title','Restart');
})
$('.cross-bot-btn').click(function(){
	if(!$('.cross-bot-btn').hasClass('hidee'))
		$('.clues li').css('color','#444444');
		initcross();
});
function setAllLocked(){
for(var i=0;i<selected_arr.length;i++)
	{
		$('.'+selected_arr[i]).addClass('lockked');	
		}	

}

$('#hint').live('click', function(){
	$('#content_sction').show();							  
								  
});

$('#close').live('click', function(){
	$('#content_sction, .overlay').hide();							  
});

};


Init();