﻿// JScript File
var count = 0;
var nextdisable = true;
var prevdisable = false;

Init = function(){
	
$('.slide_next').live('click',function(){
	prevdisable = true;								   
	if(nextdisable){	
		count++;
		if(count >= 1){
			$('.slide_next').css({'cursor':'auto'}).removeAttr('title').attr('src','imgs/next_d.png');
			nextdisable = false;
		}
		$('.slide_prev').css({'cursor':'pointer'}).attr({'title':'Previous'}).attr('src','imgs/prev.png');
		$('.slide'+(count+1)+'').css('visibility','visible');
		$(".slide_con1").animate({"marginLeft": "-=570px"}, "slow", function(){
			$('.slide'+count+'').css('visibility','hidden');
		});
		$('.page_count').html(count+1);
	}
	

});
 
$('.slide_prev').live('click',function(){  
	nextdisable = true;
	if(prevdisable){								   
		count--;
		//alert(count);
		if(count <= 0){
			$('.slide_prev').css({'cursor':'auto'}).removeAttr('title').attr('src','imgs/prev_d.png');
			prevdisable = false;
		}
		$('.slide_next').css({'cursor':'pointer'}).attr({'title':'Next'}).attr('src','imgs/next.png');
		$('.slide'+(count+1)+'').css('visibility','visible');
		$(".slide_con1").animate({"marginLeft": "+=570px"}, "slow", function(){
			$('.slide'+(count+2)+'').css('visibility','hidden');
		});
		$('.page_count').html(count+1);
	}
	
});

$('.toggleSwitch').live('click', function(){
	if($('.swfObject').hasClass('notVisible')){									  
		$('.slide').css('visibility','hidden');		
	}
	else{
		$('.slide').css('visibility','visible');		
	}
});

};
Init();
