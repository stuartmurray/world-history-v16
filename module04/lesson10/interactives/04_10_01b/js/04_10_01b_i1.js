﻿// JScript File

Init = function(){
	
$(".tab").find('.next').live('click',function(){ 
		$(".prev").css('cursor','pointer');		
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   if($(oCurrSet).next(".set").find('.dragarea .dragItem').length == 0){
		   $(this).removeAttr("disabled");
	   }
	   else{
			$(this).attr("disabled","disabled").css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)','cursor':'default'}).removeAttr('title');		
		}
	   if($(oCurrSet).next(".set").hasClass('lastset') == true || $(oCurrSet).next(".set").hasClass('lastset') == "true"){
		   $(this).css('visibility','hidden');
	   }
	   else{
		   $(this).css('visibility','visible');
		}
	   
	   $(oCurrSet).next(".set").show();
	   $(oCurrSet).hide();
		$(this).closest(".tab").find(".prev").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)','cursor':'pointer'});
		$(this).closest(".tab").find(".prev").attr({'title':'Previous'});
	   if($(oCurrSet).next(".set").next(".set").length==0){
		    animateFlag = false;
		$(this).closest(".tab").find(".next").attr("disabled", "true");	
		   $(this).closest(".tab").find(".next").css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)','cursor':'default'});	
		    $(this).closest(".tab").find(".next").removeAttr("title");
	   }
	   
	   $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
			
});
 
 	$(".tab").find('.prev').click(function(){
		$(".next").css('cursor','pointer').css('visibility','visible');
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   $(this).closest(".tab").find(".next").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)','cursor':'pointer'});
	   $(this).closest(".tab").find(".next").attr({'title':'Next'});
	   $(oCurrSet).prev(".set").show();
	   $(oCurrSet).hide();
	   if($(oCurrSet).prev(".set").prev(".tab .set").length==0){
		   $(this).closest(".tab").find(".prev").attr("disabled", "true");
		    $(this).closest(".tab").find(".prev").removeAttr("title");
		   $(this).closest(".tab").find(".prev").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	
	   }
	   
	   $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
		
 });

$('.Img1').live('click', function(){
	var index = $(this).attr('class');	
	index = index.replace("Img1 ","");
	$('.popup_txt').html($('.'+index+'_txt').html());
	$('.popup, .overlay').show();
									  
});

$('.popup_close').live('click', function(){
	$('.popup_txt').html('');
	$('.popup, .overlay').hide();
									  
});

}
Init();