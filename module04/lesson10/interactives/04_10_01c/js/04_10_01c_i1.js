﻿// JScript File

Init = function(){

 $(".tab").find('.nxt').click(function(){  
		$(".prev").css('cursor','pointer');		
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   $(oCurrSet).next(".set").show();
	   $(oCurrSet).hide();
		$(this).closest(".tab").find(".prev").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)'}).attr({'title':'Previous'});
		
	   if($(oCurrSet).next(".set").next(".set").length==0){
		 $(this).closest(".tab").find(".nxt").attr("disabled", "true").removeAttr('title');
		   $(this).closest(".tab").find(".nxt").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	;
	   }
	 	//pageNo = 1;
	//  pageNo++;
	   
	$(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
			
});
 
 	$(".tab").find('.prev').click(function(){
		$(".nxt").css('cursor','pointer');		
		
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	  $(this).closest(".tab").find(".nxt").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)'}).attr({'title':'Next'});
	   $(oCurrSet).prev(".set").show();
	   $(oCurrSet).hide();
	   if($(oCurrSet).prev(".set").prev(".tab .set").length==0){
		  $(this).closest(".tab").find(".prev").attr("disabled", "true").removeAttr('title');
		   $(this).closest(".tab").find(".prev").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	
	   }
	  // pageNo--;
	   
		$(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
		
 });

	$('a.lightbox').each(function(){
		$(this).lightBox({fixedNavigation:true});
		$(this).append($(document.createElement("div")).addClass("magGlass"));
		$('.magGlass').css({left:'274px', top:'270px'});
	});
	 
 }
 Init();