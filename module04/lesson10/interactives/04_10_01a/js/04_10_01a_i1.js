﻿// JScript File
var dragRevert = true;


Init = function(){

 $(".tab").find('.next').live('click',function(){ 
		
		
		
		
		$(".prev").css('cursor','pointer');		
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   if($(oCurrSet).next(".set").find('.dragarea .dragItem').length == 0){
		   $(this).removeAttr("disabled");
	   }
	   else{
			$(this).attr("disabled","disabled").css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)','cursor':'default'}).removeAttr('title');		
		}
	   if($(oCurrSet).next(".set").hasClass('lastset') == true || $(oCurrSet).next(".set").hasClass('lastset') == "true"){
		   $(this).css('visibility','hidden');
	   }
	   else{
		   $(this).css('visibility','visible');
		}
	   
	   $(oCurrSet).next(".set").show();
	   $(oCurrSet).hide();
		$(this).closest(".tab").find(".prev").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)','cursor':'pointer'});
		$(this).closest(".tab").find(".prev").attr({'title':'Previous'});
	   if($(oCurrSet).next(".set").next(".set").length==0){
		    animateFlag = false;
		$(this).closest(".tab").find(".next").attr("disabled", "true");	
		   $(this).closest(".tab").find(".next").css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)','cursor':'default'});	
		    $(this).closest(".tab").find(".next").removeAttr("title");
	   }
	   
	   $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
			
});
 
 	$(".tab").find('.prev').click(function(){
		$(".next").css('cursor','pointer').css('visibility','visible');
	   var oCurrSet = $(this).closest(".tab").find(".set:visible");
	   $(this).closest(".tab").find(".next").removeAttr("disabled").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)','cursor':'pointer'});
	   $(this).closest(".tab").find(".next").attr({'title':'Next'});
	   $(oCurrSet).prev(".set").show();
	   $(oCurrSet).hide();
	   if($(oCurrSet).prev(".set").prev(".tab .set").length==0){
		   $(this).closest(".tab").find(".prev").attr("disabled", "true");
		    $(this).closest(".tab").find(".prev").removeAttr("title");
		   $(this).closest(".tab").find(".prev").css({'cursor':'default'}).css({'opacity':'0.3','filter':'alpha(opacity = 30)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=30)'});	
	   }
	   
	   $(this).closest(".tab").find(".pageNo").html(($(this).closest(".tab").find(".set").index($(this).closest(".tab").find(".set:visible")) +1)+ "/"+ ($(this).closest(".tab").find(".set").length));
		
 });


	InitDrag();
	
function InitDrag(){
	$(".ui-draggable").draggable({containment:'.contentMiddleBg', stack: ".dragItem" , revert: function(){if(dragRevert == true){return true;}else{dragRevert = true;}}});
}
	
	$(".dropItem").each(function(){
		$(this).droppable({
			drop:function(event, ui){
				if($(this).html() == ""){
					var DragEleid = ui.draggable.attr("id");
					var Dragid = ui.draggable.attr("dragid");
					var Dropid = $(this).attr("dropid");
					if(Dragid == Dropid){
						$('#'+DragEleid).css({'left':'0px','top':'0px','zIndex':'0','cursor':'auto','background':'#99FFCC'}).draggable('disable');				
						$(this).html(ui.draggable);
						showFeed();
						enableNext();
					}
				}
			}
		});
	});

if(/iphone|ipod|ipad|android/i.test(navigator.userAgent)){
		init();
}

function showFeed(){
	$('.overlay, .feed_div, .corctFB').show();
}

$('.feed_close').live('click', function(){
	$('.overlay, .feed_div, .corctFB').hide();									
});

function enableNext(){
	var oCurrSet = $(".tab").find(".set:visible");
	//&& $(oCurrSet).hasClass('.lastset') == "false"
	if($(oCurrSet).find('.dragarea').find('.dragItem').length == 0 ){
		$(".next").css({'opacity':'1','filter':'alpha(opacity = 100)','-ms-filter':'progid:DXImageTransform.Microsoft.Alpha(Opacity=100)','cursor':'pointer'}).removeAttr("disabled").attr('title','Next');	
	}
}

function init(){
	var a=0;
	$(".dragItem").each(function(){
		var b=$(this).attr("id");
		var c=document.getElementById(b);
		a++;
		c.addEventListener("touchstart",touchHandler,true);
		c.addEventListener("touchmove",function(e){e.preventDefault();},true);
		c.addEventListener("touchmove",touchHandler,true);
		c.addEventListener("touchend",touchHandler,true);
		c.addEventListener("touchcancel",touchHandler,true);
	});
}

function touchHandler(a){
	var b=a.changedTouches,c=b[0],d="";
	switch(a.type){
		case"touchstart":
			d="mousedown";
			break;
		case"touchmove":
			d="mousemove";
			break;
		case"touchend":
			d="mouseup";
			break;
		default:
			return
	}
	var e=document.createEvent("MouseEvent");
	e.initMouseEvent(d,true,true,window,1,c.screenX,c.screenY,c.clientX,c.clientY,false,false,false,false,0,null);
	c.target.dispatchEvent(e);
}

};

Init();