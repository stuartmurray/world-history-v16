﻿// JScript File
var dragRevert = true;
var submitDragFlag = false;
var rightDrop = 0;
var chanceCount = 0;
var rightdrag = [];
var wrongdrop = [];
var wrongdrag = [];

var feedTxt = ["<span class='neg_txt1'>Almost! Try again.</span> <img src='../module01/01_06_01i_i1/web/imgs/tryagain_btn.png' class='try_again' title='Try again' alt='Try again' />","<span class='pos_txt'>That’s right! You have matched each item correctly. </span>","<span class='neg_txt2'>Take a look at the correct matching.</span>"]

Init = function(){

	
	InitDrag();
	
function InitDrag(){
	if($('.drag_ul1').length == 0){
		var oCurrActivity = $('.drag_ul').clone();
		$(oCurrActivity).removeAttr('class').addClass('drag_ul1');
		$(".footer_div").append(oCurrActivity);
	}
	if($('.drag_ul2').length == 0){
		var oCurrActivity = $('.drag_ul').clone();
		$(oCurrActivity).removeAttr('class').addClass('drag_ul2');
		$(".footer_div").append(oCurrActivity);
	}
	$(".ui-draggable").draggable({containment:'.con_div', stack: ".drag_ul li" , revert: function(){if(dragRevert == true){return true;}else{dragRevert = true;}}});
	
	if(/iphone|ipod|ipad|android/i.test(navigator.userAgent)){
		init();
}

}
	
	$(".drop_ul>li").each(function(){
		$(this).droppable({
			drop:function(event, ui){
				if($(this).html() == ""){
					var DragEleid = ui.draggable.attr("id");
					$('#'+DragEleid).css({'left':'0px','top':'0px','zIndex':'0'});				
					$(this).html(ui.draggable);
					enableSubmit();
				}
			}
		});
	});


function enableSubmit(){
	if($('.drag_ul li').length == 0){
		$('#submit').css({'opacity':'1','cursor':'pointer'}).attr('title','Submit');
		submitDragFlag = true;
	}
}

$('#submit').live('click', function(){
	if(submitDragFlag){
		submitDragFlag = false;
		$(this).css({'opacity':'0.6','cursor':'auto'}).removeAttr('title');
		$('.dropItem').each(function(){
			var dragid = $(this).children().attr('id');
			var dropid = $(this).attr('id');
			if($('#'+dragid).attr('dragid') == $('#'+dropid).attr('dropid')){
				rightDrop++;
				rightdrag.push(dragid);
			}else if($('#'+dropid).html() != ""){
				wrongdrop.push(dropid);
				wrongdrag.push(dragid);
			}
		});
		showFeedBack();
	}
	
});

function showFeedBack(){
	$('.overlay').show();	
	if(rightDrop == $('.dropItem').length){
		$('.feedback').html(feedTxt[1]).show();
		showResult();
	}else{
		chanceCount++;
		switch(chanceCount){
			case 1:
				$('.feedback').html(feedTxt[0]).show();
				showResult();
			break;
			
			case 2:
				chanceCount = 0;
				$('.feedback').html(feedTxt[2]).show();
				showCorrectAns();
			break;
		}
	}
}


function showResult(){
	for(i = 0; i < rightdrag.length; i++){
		$('#'+rightdrag[i]).css('background','#D6EEFE url(../module01/01_03_05_8a_i1/web/imgs/dragRight.png) no-repeat 148px 22px');	
	}
	for(i = 0; i < wrongdrag.length; i++){
		$('#'+wrongdrag[i]).css('background','#D6EEFE url(../module01/01_03_05_8a_i1/web/imgs/dragWrong.png) no-repeat 148px 22px');	
	}
}

$('.try_again').live('click', function(){
	$('.overlay, .feedback').empty().hide();
	for( i = 0; i < wrongdrop.length; i++){
		var returnPos = $('#'+wrongdrop[i]+' li').attr('id');
		returnPos  = returnPos.replace('drag',' ');
		returnPos = returnPos - 1;
		$('.drag_ul div:eq('+returnPos+')').append($('#'+wrongdrop[i]).html());
		$('#'+wrongdrop[i]).html('');
	}
	$('.drag_ul div li, .drop_ul li li').removeAttr('style');
	$('.drop_ul li li').css({'background':'#D6EEFE url(../module01/01_03_05_8a_i1/web/imgs/dragRight.png) no-repeat 148px 22px','cursor':'auto'}).removeClass('ui-draggable').draggable("disable");	
	wrongdrop = [];
	wrongdrag = [];
	rightdrag = [];
	rightDrop = 0;
	InitDrag();
});

function showCorrectAns(){
	$('.dropItem').html('');
	for( i = 1; i <= $('.dropItem').length; i++){
		for( j = 1; j <= $('.drag_ul1 div').length; j++){
			var iscrct = $('#drop'+i+'').attr('iscorrect');
			var iscrct1 = $('.drag_ul1 .drag'+j+'').attr('iscorrect');
			//alert(('#drop'+i+'') +'-----'+ ('.drag_ul1 .drag'+j+''));
			if($('#drop'+i+'').attr('dropid') == $('.drag_ul1 .drag'+j+'').attr('dragid') && (iscrct == "false") && (iscrct1 == "false")){
				$('#drop'+i+'').html($('.drag_ul1 .drag'+j+'').css('background','#D6EEFE url(../module01/01_03_05_8a_i1/web/imgs/dragRight.png) no-repeat 148px 22px'));	
				$('#drop'+i+'').attr('iscorrect','true');
				$('.drag_ul1 .drag'+j+'').attr('iscorrect','true');
			}
		}
	}
	$('#submit').hide();
	$('#restart').show();
	
}

$('#restart').live('click', function(){
	$('.overlay, .feedback').empty().hide();
	$('.dropItem').html('').attr('iscorrect','false');
	$('.drag_ul').html($('.drag_ul2').html());
	$('.drag_ul1, .drag_ul2').remove();
	$('#restart').hide();
	$('#submit').show();
	wrongdrop = [];
	wrongdrag = [];
	rightdrag = [];
	chanceCount= 0;
	rightDrop = 0;
	InitDrag();
	//Init();
});

function init(){
	var a=0;
	$(".drag_ul div>li").each(function(){
		var b=$(this).attr("id");
		var c=document.getElementById(b);
		a++;
		c.addEventListener("touchstart",touchHandler,true);
		c.addEventListener("touchmove",function(e){e.preventDefault();},true);
		c.addEventListener("touchmove",touchHandler,true);
		c.addEventListener("touchend",touchHandler,true);
		c.addEventListener("touchcancel",touchHandler,true);
	});
}

function touchHandler(a){
	var b=a.changedTouches,c=b[0],d="";
	switch(a.type){
		case"touchstart":
			d="mousedown";
			break;
		case"touchmove":
			d="mousemove";
			break;
		case"touchend":
			d="mouseup";
			break;
		default:
			return
	}
	var e=document.createEvent("MouseEvent");
	e.initMouseEvent(d,true,true,window,1,c.screenX,c.screenY,c.clientX,c.clientY,false,false,false,false,0,null);
	c.target.dispatchEvent(e);
}
};

Init();