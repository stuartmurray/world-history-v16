﻿// JScript File

Root=null;
UserAttempt=0;

Init = function(){
    Root = $(".MultiChoice");
    $(Root).find(".MCSet:first").show();
    
    SetPager(); 

    $(Root).find("input").click(function(){
        $(this).closest(".MCSet").find("input").removeClass("attended");
        $(this).addClass("attended");
        $(Root).find(".MCSubmit").removeClass("MCDisable").attr({'title':'Submit'});
    });
     
    $(Root).find(".MCTryAgain").click(function(){
        var oCurrSet;
        oCurrSet=$(this).closest(".MCSet");
        $(oCurrSet).find("input").removeAttr("checked");
        $(oCurrSet).find(".MCOverlay").hide();
        $(oCurrSet).find(".MCFeedback").hide();
        $(oCurrSet).find(".MCFeedback .MCFeedtxt span").hide();
        UserAttempt+=1;
    });
    $(Root).find(".MCSubmit").click(function(){
        if(!$(this).hasClass("MCDisable")){
            var oCurrSet;
            
            UserAttempt+=1;
            oCurrSet = $(Root).find(".MCSet:visible");
			$(oCurrSet).attr('attend','yes');		
            $(this).addClass("MCDisable").removeAttr('title');
            $(oCurrSet).find(".MCFeedback").show();
            $(oCurrSet).find(".MCOverlay").show();
            
            if($(oCurrSet).find("input.attended").hasClass("MCcrct")){
                $(oCurrSet).find(".MCFeedback").find(".MCC").show();
                $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").hide();
                $(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCCorrect");
                if($(Root).find(".MCSet:visible").next(".MCSet").length==0){
                    $(Root).find(".MCRestart").show();
					$(".MCReverse, .MCForward").hide();
                    $(this).hide();
                }
                else{
                    $(Root).find(".MCForward").show();
                }
            }
            else{
/*				$(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCinCorrect");
				$(oCurrSet).find(".MCFeedback").find(".MCINC1").show();
				$(oCurrSet).find(".MCFeedback").find(".MCTryAgain").show();
*/				//alert(UserAttempt);
                $(oCurrSet).find("input.attended").parent().find(".MCFeedImg").addClass("MCinCorrect");
                if(UserAttempt==1){
                    $(oCurrSet).find(".MCFeedback").find(".MCINC1").show();
                    $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").show();
                }
                else{
                    $(oCurrSet).find(".MCFeedback").find(".MCINC2").show();
                    $(oCurrSet).find(".MCFeedback").find(".MCTryAgain").hide();
                    $(oCurrSet).find("input.MCcrct").parent().find(".MCFeedImg").addClass("MCCorrect");
                    if($(Root).find(".MCSet:visible").next(".MCSet").length==0){
                        $(Root).find(".MCRestart").show();
						$(".MCReverse, .MCForward").hide();
                        $(this).hide();
                    }
                    else{
                        $(Root).find(".MCForward").show();
                    }
                }
				}
        }
    });
    $(Root).find(".MCForward").click(function(){
        var oCurrSet;
        $(".MCReverse").show();
        UserAttempt=0;
		oCurrSet = $(Root).find(".MCSet:visible");
		
		/*$(oCurrSet).next('.MCSet').find('.MCFeedback').css('display') == "block" ||*/
		if($(oCurrSet).next('.MCSet').attr('attend') == "yes"){
			$(this).show();
		}
		else if($(oCurrSet).next('.MCSet').hasClass('lastQues')){
			$(this).hide(); 
			//alert('last');
		}
		else 
		{ 
			$(this).hide(); 
		}
		$(oCurrSet).next(".MCSet").show();
        $(oCurrSet).hide();
        SetPager();
    }); 
$(Root).find(".MCReverse").click(function(){
		
        oCurrSet = $(Root).find(".MCSet:visible");
		if($(oCurrSet).prev(".MCSet").hasClass('firstQues')){
			$(this).hide();
		}

        $(oCurrSet).prev(".MCSet").show();
        $(oCurrSet).hide();
        SetPager();
		$(".MCForward").show();
	});
/*    $(Root).find(".MCForward").click(function(){
        var oCurrSet;
        
        UserAttempt=0;
        $(this).hide();
        oCurrSet = $(Root).find(".MCSet:visible");
        $(oCurrSet).next(".MCSet").show();
        $(oCurrSet).hide();
        SetPager();
    }); 
*/     
    $(Root).find(".MCRestart").click(function(){
		$(".MCReverse, .MCForward").hide();	
        UserAttempt=0;
        $(this).hide();
		$('.MCSet').attr('attend','no');
        $(Root).find(".MCSet:visible").hide();
        $(Root).find(".MCSet:first").show();
        $(Root).find(".MCFeedback").hide()
        $(Root).find(".MCOverlay").hide();
        $(Root).find(".MCFeedback").find("span").hide()
        $(Root).find(".MCCorrect, .MCinCorrect").removeClass("MCCorrect").removeClass("MCinCorrect");
        $(Root).find("input").removeAttr("checked");
        $(Root).find(".MCSubmit").show();
        SetPager();
    });
};

SetPager=function(){
    $(Root).find(".MCPager").text( ($(Root).find(".MCSet").index($(Root).find(".MCSet:visible"))+1) + "/" + $(Root).find(".MCSet").length);
};
Init();