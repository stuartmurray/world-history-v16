﻿$(document).ready(function () {
    $('.ui_edge_buttons>.ui_submit').on('click', function () {
        $('.drop').each(function () {
            if ($(this).find('.bin').find('ul').find('li').attr("data-label") == $(this).attr("data-label")) {
                $(this).addClass('bordergreen');
            } else {
                $(this).addClass('borderred');
            }
        });
    });
    $('.drop').bind('drop', function () {
        $(this).droppable('disable').css('opacity', '1');
        setTimeout(function () {
            $('.drop').each(function () {

                if ($(this).find('li').length > 0) {
                    $(this).css('background', '#A09E9E');
                    if ($(this).find('.bin').hasClass('binred')) {
                        $(this).find('.bin').removeClass('binred').addClass('binred1');
                    }
                    if ($(this).find('.bin').hasClass('binblue')) {
                        $(this).find('.bin').removeClass('binblue').addClass('binblue1');
                    }
                }
            });
        }, 100);
    });
    $('.ui_ddremove').bind('click', function () {
        setTimeout(function () {
            $('.drop').each(function () {
                if (!$(this).find('li').length > 0) {
                    $(this).css('background', 'transparent');
                    $(this).droppable('enable').removeClass('borderred');
                    if ($(this).find('.bin').hasClass('binred1')) {
                        $(this).find('.bin').removeClass('binred1').addClass('binred');
                    } else if ($(this).find('.bin').hasClass('binblue1')) {
                        $(this).find('.bin').removeClass('binblue1').addClass('binblue');
                    } 
                }
            });
        }, 200);
    });
   
    $('.ui_reset').bind('click', function () {
        $('.drop').each(function () {
            $(this).css('background', 'transparent');
            $(this).droppable('enable').removeClass('borderred').removeClass('bordergreen');
        });
        $('.bin').each(function () {
            if ($(this).hasClass('binred1')) {
                $(this).removeClass('binred1').addClass('binred');
            } else if ($(this).hasClass('binblue1')) {
                $(this).removeClass('binblue1').addClass('binblue');
            }
        });
    });



});