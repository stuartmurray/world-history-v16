﻿// JScript File

var dragRevert = true;
var count = 0;
var con_Txt = ["I left <b>Tangier</b>, my birthplace, on Thursday, 2nd Rajab 725 [June 14, 1325], being at that time twenty-two years of age [22 lunar years; 21 and 4 months by solar reckoning], with the intention of making the Pilgrimage to the Holy House [at Mecca] and the Tomb of the Prophet [at Medina].", "It is said that in <b>Cairo</b> there are twelve thousand water-carriers who transport water on camels, and thirty thousand hirers of mules and donkeys, and that on the Nile there are thirty-six thousand boats belonging to the Sultan and his subjects which sail upstream to Upper Egypt and downstream to Alexandria and Damietta, laden with goods and profitable merchandise of all kinds.","We then reached <b>Jerusalem</b> (may God ennoble her!), third in excellence after the two holy shrines of Mecca and Medina and the place whence the Prophet was caught up into heaven. Its walls were destroyed by the illustrious King Saladin and his Successors, for fear lest the Christians should seize it and fortify themselves in it.","We set out at night... with hearts full of joy at reaching the goal of our hopes, and in the morning arrived at the City of Surety, <b>Mecca</b> (may God ennoble her!), where we immediately entered the holy sanctuary and began the rites of pilgrimage.","Thence we travelled to <b>Baghdad</b>, the Abode of Peace and Capital of Islam. Here there are two bridges like that at Hilla on which the people promenade night and day, both men and women. The town has eleven cathedral mosques, eight on the right bank and three on the left, together with very many other mosques and madrasas, only the latter are all in ruins.","We spent only one night at <b>Tabriz</b>. Next day the amir received an order from the sultan to rejoin him, so I returned along with him, without having seen any of the learned men there [in Tabriz]. On reaching the camp the amir told the sultan about me and introduced me into his presence. The sultan asked me about my country, and gave me a robe and a horse.","I travelled thence to <b>Aden</b>, the port of Yemen, on the coast of the ocean. … It is the port of the Indians, and to it come large vessels from Kinbayat [Cambay], Kawlam [Quilon], Calicut and many other Malabar ports [on the south-west coast of India]. There are Indian merchants living there, as well as Egyptian merchants. Its inhabitants are all either merchants, porters, or fishermen.","On leaving Zayla we sailed for fifteen days and came to Maqdasha [<b>Mogadishu</b>], which is an enormous town. Its inhabitants are merchants and have many camels, of which they slaughter hundreds every day [for food]. When a vessel reaches the port, it is met by sumbuqs, which are small boats, in each of which are a number of young men, each carrying a covered dish containing food. He presents this to one of the merchants on the ship saying "+"This is my guest,"+" and all the others do the same.","Our entry into <b>Constantinople</b> the Great was made about noon or a little later, and they rang their bells until the very skies shook with the mingling of their sounds. When we reached the fist gate of the king's palace we found there about a hundred men, with an officer on a platform, and I heard them saying "+"Sarakinu, Sarakinu,"+" ["+"Saracen, Saracen,"+"] which means Muslims. They would not let us enter.","Thence [from Malaga] I went to on the city of Gharnata [<b>Granada</b>], the metropolis of Andalusia and the bride of its cities. … The king of Gharnata at the time of my visit was Sultan Abu'l-Hajjaj Yusuf. I did not meet him on account of an illness from which he was suffering, but the noble, pious, and virtuous woman, his mother, sent me some gold dinars, of which I made good use.","Thence we went on to Tumbuktu [<b>Timbuktu</b>], which stands four miles from the river [Niger]. Most of its inhabitants are of the Massufa tribe, wearers of the face-veil. Its governor is called Farba Musa. I was present with him one day when he had just appointed one of the Massufa to be amir of a section. He assigned to him a robe, a turban, and trousers, all of them of dyed cloth, and bade him sit upon a shield, and the chiefs of his tribe raised him on their heads."];

var prevDisable = false;
var nextDisable = true;
var attrDragid, attrDropid, Dragid, Dropid;
var attemptCnt = 0;

var feedTxt = ["<span class='neg_txt1'>Sorry but that’s incorrect. Please, try again!</span> <img src='imgs/tryagain_btn.png' class='try_again' />","<span class='pos_txt'>Great job! That's correct! Where did Ibn Battuta go next?</span> <span class='feed_close'>x</span>","<span class='neg_txt2'>Take a look at the correct matching.</span> <span class='feed_close'>x</span>","<span class='pos_txt'>Fantastic! Ibn Battuta ended his journey soon after he reached Timbuktu.</span> <span class='feed_close'>x</span>"]

fnInit = function(){

$('.con_head span').html('1'); // Initial Load
$('.con_txt').html('"'+con_Txt[0]+'"');
InitDrag();

$('.button-n').live('click', function(){
	if(nextDisable){
		prevDisable = true;
		$('.button-p').css({'opacity':'1','cursor':'pointer'}).attr('title','Previous');
		count++;
		if(count == 10){
			$('.button-n').css({'opacity':'0.5','cursor':'auto'}).removeAttr('title');
			nextDisable = false;
		}
		$('.con_head span').html(count+1); 
		$('.con_txt').html('"'+con_Txt[count]+'"');
		InitDrag();
	}
});
	
$('.button-p').live('click', function(){
	if(prevDisable){
		nextDisable = true;
		$('.button-n').css({'opacity':'1','cursor':'pointer'}).attr('title','Next');
		count--;	
		if(count == 0){
			$('.button-p').css({'opacity':'0.5','cursor':'auto'}).removeAttr('title');
			prevDisable = false;
		}
		$('.con_head span').html(count+1); 
		$('.con_txt').html('"'+con_Txt[count]+'"');
		//resetDrag();
		InitDrag();
	}
});
	
function resetDrag(){
	var resetDrop = $('.dragItem').eq(count).attr('dragid');
	$('.dragItem').eq(count).show();
	for(i = 0; i < $('.dropItem').length; i++){
		if( $('.dropItem').eq(i).attr('dropid') == resetDrop){
			$('.dropItem').eq(i).html('');
		}
	}
	attemptCnt = 0;
}
	

function InitDrag(){
	
	$('.drag_ul li').removeClass('ui-draggable').draggable("disable").css({'opacity':'0.6','cursor':'auto'});
	$('.drag_ul li').eq(count).addClass('ui-draggable').css({'opacity':'1','cursor':'pointer'}).draggable("enable");
	$(".ui-draggable").draggable({containment:'.drag_droparea', stack: ".drag_ul li" , revert: function(){if(dragRevert == true){return true;}else{dragRevert = true;}}});
}



$(".drop_ul>li").each(function(){
	$(this).droppable({
		drop:function(event, ui){
			attrDragid = ui.draggable.attr("dragid");
			attrDropid = $(this).attr("dropid");
			Dragid = ui.draggable.attr("id");
			Dropid = $(this).attr("id");
			doDrop();
		}
	});
});

if(/iphone|ipod|ipad|android/i.test(navigator.userAgent)){
		init();
}

function doDrop(){
	if(attrDragid == attrDropid){
		$("#"+Dropid).html($("#"+Dragid).html());
		$("#"+Dragid).hide();
		if(count < 10){
			$('.feedback').html(feedTxt[1]).show();
		}
		else{
			$('.feedback').html(feedTxt[3]).show();
		}
		attemptCnt = 0;
	}else{
		attemptCnt++;
		switch(attemptCnt){
			case 1:
				$('.feedback').html(feedTxt[0]).show();
			break;
			case 2:
				$('.feedback').html(feedTxt[2]).show();
				showCorrectAns();
				attemptCnt = 0;
			break;
		}
		
	}
	$('.overlay').show();
}


$('.feed_close, .try_again').live('click', function(){
	$('.feedback, .overlay').empty().hide();
});

function showCorrectAns(){
	for( i = 0; i < $('.drop_ul li').length; i++){
		if( $('.drop'+i+'').attr('dropid') == attrDragid){
			$('.drop'+i+'').html($("#"+Dragid).html());
			$("#"+Dragid).hide();
		}
	}
}


console.log("sss");



function init(){
	var a=0;
	$(".drag_ul div>li").each(function(){
		var b=$(this).attr("id");
		var c=document.getElementById(b);
		a++;
		c.addEventListener("touchstart",touchHandler,true);
		c.addEventListener("touchmove",function(e){e.preventDefault();},true);
		c.addEventListener("touchmove",touchHandler,true);
		c.addEventListener("touchend",touchHandler,true);
		c.addEventListener("touchcancel",touchHandler,true);
	});
}

function touchHandler(a){
	var b=a.changedTouches,c=b[0],d="";
	switch(a.type){
		case"touchstart":
			d="mousedown";
			break;
		case"touchmove":
			d="mousemove";
			break;
		case"touchend":
			d="mouseup";
			break;
		default:
			return
	}
	var e=document.createEvent("MouseEvent");
	e.initMouseEvent(d,true,true,window,1,c.screenX,c.screenY,c.clientX,c.clientY,false,false,false,false,0,null);
	c.target.dispatchEvent(e);
}

};

fnInit();