﻿$(document).ready(function () {
    $('.customPopUp').hide();
    $('.customDivCall').unbind('click').bind('click', function () {
        if ($(this).hasClass('christianityMap')) {
            $('.customPopUp .cont').html("<div class='hd'>Sunni Muslims</div><ul class='unListed'><li>The Sunni Muslims believed that leadership should pass to people accepted by the Muslim community, not necessarily a direct relative of Muhammad</li><li>About 90% of today’s Muslims are Sunni</li></ul>");
        } else {
            $('.customPopUp .cont').html("<div class='hd'>Shi’a Muslims</div><ul class='unListed'><li>The Shi’a Muslims believed that leadership should pass through descendants of Muhammad or his blood relatives</li><li>About 10% of today’s Muslims are Shi’a</li></ul>");
        }
        $('.customPopUp').show();
        $('.close1').unbind('click').bind('click', function () {
            $('.customPopUp').hide();
        });
    });
   

});