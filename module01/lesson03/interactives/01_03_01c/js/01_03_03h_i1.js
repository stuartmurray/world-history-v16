﻿// JScript File

var pop_head = ["Spread of Islam, 612-632","Spread of Islam, 632-661","Spread of Islam, 661-750","Spread of Islam, 750-1258","Spread of Islam, 1258-1683"];
var pop_img = ["1_3_3_8_map_04","1_3_3_8_map_03","1_3_3_8_map_02","1_3_3_8_map_01","1_3_3_8_map_05"];
var pop_txt = ['When Muhammad first received his revelations in 612, authorities in Mecca were reluctant to accept his monotheistic ideas. Yet, by 632, Muhammad and his followers had come to control a large area of the Arabian Peninsula. Tribes that came under Muhammad’s rule adopted Islam. Then, in 632, Muhammad died.','The first four rulers to follow Muhammad were called the Four Rightly Guided Caliphs, or rulers. Under their rule, Muslim armies defeated the Persians and the Byzantines. As a result, they took control of the Arabian Peninsula as well as Persia, Syria and much of Egypt, building the foundations of an Arab Empire.','In 661, the last Rightly Guided Caliph was murdered. This event not only resulted in a split within the Islamic world but also gave rise to the powerful Umayyad Caliphate. Umayyad armies ranged across North Africa into what is now Spain on the Iberian Peninsula as well as farther east into Central Asia, conquering northern India.','In 747, subject peoples revolted against Umayyad rule, and a new family, the Abbasids, rose to power. One Umayyad ruler, however, escaped and established a new empire in Spain. Persian and Turkic rulers also adopted Islam even as they regained control over lands in Southwest Asia. Meanwhile, Arab merchants carried Islam to powerful trading empires in West and East Africa. From India, merchants also carried Islam into China and Southeast Asia. Though these areas did not become part of the various Muslim empires, they did become Islamic.','By the 13th century, Islam was well-established in Southwest Asia and North Africa. In the coming centuries, several powerful empires arose. In what is now Turkey, the Ottomans conquered Constantinople and extended their empire into not only Arab and Persian lands but also north into Europe. To the east, a people known as the Mongols adopted Islam and gave rise to a new empire in India. By the 17th century, Islam had become the majority religion in the shaded lands and had spread farther through trade.'];

var index;

Init = function(){

$('.select_ul li').live('click', function(){
	index = $(this).index();
	showPopup();								
});

function showPopup(){
	$('.popup_head').html(pop_head[index]);
	$('.popup_img').css('background','url(imgs/'+pop_img[index]+'.png) no-repeat');
	if( index == 1 || index == 2 || index == 3){
		$('.popup_Txt').html(pop_txt[index]);
	}
	else{
		$('.popup_Txt').html(pop_txt[index]);
	}
	$('.pop_up').show();
}

$('.popup_close').live('click', function(){
	$('.pop_up').hide();								
});

};
Init();
