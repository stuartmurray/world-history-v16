﻿var count = 0;
var currentSlide = 1;
$(document).ready(function () {

    $(document).find('.ui_slide_mc').each(function () {
        count++;

    });
    $('.ui_edge_buttons>.ui_submit').on('click', function () {
        setTimeout(function () {
            if ($('#01_05_04_slide0' + currentSlide).find('.correct').parent().css('display') == 'block' || $('#01_05_04_slide0' + currentSlide).find('.failed').parent().css('display') == 'block') {
                $('.img_txt').css('display', 'none');
                $('#puzzle_imgCont' + currentSlide).show();
                if (currentSlide == "4") {
                    $('#imgDesc').show();
                    $('.ui_edge_buttons>.ui_submit').hide();
                } else {
                    $('#imgDesc').hide();
                    $('.ui_edge_buttons>.ui_submit').show();
                
                }
            }

        }, 100);

    });
    $('.ui-navigation>li>.next').on('click', function () {
        currentSlide++;
        return;
    });

    $('.ui-navigation>li>.prev').on('click', function () {
        currentSlide--;
        if (currentSlide == "4") {
            $('#imgDesc').show();
            $('.ui_edge_buttons>.ui_submit').hide();
        } else {
            $('#imgDesc').hide();
            $('.ui_edge_buttons>.ui_submit').show();

        } return;
    });
    $('.ui-navigation>li>.restart').on('click', function () {
        currentSlide = 0;
        $('.img_txt').css('display', 'block');
        $('.puzzle_imgCont').find('img').each(function () {
            $(this).css('display', 'none');
        });
        $('#imgDesc').hide();

        $('.ui_edge_buttons>.ui_submit').show();
        return;
    });



});