﻿$(document).ready(function () {
    $('.customPopUp').hide();
    $('.customDivCall').unbind('click').bind('click', function () {
        if ($(this).hasClass('christianityMap')) {
            $('.customPopUp .cont').html("<div class='hd'>Christianity</div><ul class='unListed'>Christianity has approximately 2,286,000,000 followers worldwide. This is 33 percent of the world. Christians are concentrated in North and South America, Europe, Sub-Saharan Africa, and Australia.</ul>");
        } else if ($(this).hasClass('islamMap')) {
            $('.customPopUp .cont').html("<div class='hd'>Islam</div><ul class='unListed'>Islam has approximately 1,524,000,000 followers worldwide. This is 22 percent of the world. Followers of Islam, known as Muslims,  are concentrated in North Africa, Western and Central Asia, and Indonesia.</ul>");
        } else if ($(this).hasClass('hinduismMap')) {
            $('.customPopUp .cont').html("<div class='hd'>Hinduism</div><ul class='unListed'>Hinduism has approximately 901,000,000 followers worldwide. This is 13 percent of the world. Hindus are concentrated in South Asia.</ul>");
        } else if ($(this).hasClass('buddhismMap')) {
            $('.customPopUp .cont').html("<div class='hd'>Buddhism</div><ul class='unListed'>Buddhism has approximately 484,000,000 followers worldwide. This is 7 percent of the world. Buddhists are concentrated in China, Mongolia, Vietnam, Laos, Cambodia, Thailand, Korea, and Japan.</ul>");
        } else if ($(this).hasClass('judaismMap')) {
            $('.customPopUp .cont').html("<div class='hd'>Judaism</div><ul class='unListed'>Judaism has approximately 15,000,000 followers worldwide. This is less than 0.25 percent of the world. Followers of Judaism can be found most everywhere but are concentrated in Israel and North America.</ul>");
        } else if ($(this).hasClass('chinese')) {
            $('.customPopUp .cont').html("<div class='hd'>Chinese Religions</div>");
        } else if ($(this).hasClass('nonReligious')) {
            $('.customPopUp .cont').html("<div class='hd'>Non-Religious</div>");
        } else if ($(this).hasClass('tribal')) {
            $('.customPopUp .cont').html("<div class='hd'>Tribal Religions</div>");
        }
        $('.customPopUp').show();
        $('.close1').unbind('click').bind('click', function () {
            $('.customPopUp').hide();
        });
    });
   

});