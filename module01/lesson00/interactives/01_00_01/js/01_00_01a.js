var count = 1;
var clked = 0;

var CuurentSlide = 0;
$(document).ready(function () {

    $('#imgPlay').live('click', function () {
        $('.playerOver').css('display', 'none');
        $('#mep_0').find('.mejs-play').find('button').trigger('click');
    });

    countmessage();

    $('.ui-navigation .next').bind('click', function () {
        if (clked != 0) {
            return;
        }
        clked = 1;
        count++;
        CuurentSlide++;
        countmessage();
        setTimeout(function () {
            clked = 0;
        }, 400);

    });
    $('.ui-navigation .prev').bind('click', function () {
        if (clked != 0) {
            return;
        }
        clked = 1;
        count--;
        countmessage();
        setTimeout(function () {
            clked = 0;
        }, 400);

    });
    $('.ui-navigation .restart').bind('click', function () {
        count = 1;
        countmessage();
    });
});

function countmessage() {
    
    if (count == 1 && CuurentSlide==0) {
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! My name is Ali. And this is my friend Soo-jin.");

    } else if (count == 1 && CuurentSlide != 0) {
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Hi, students! My name is Ali. And this is my friend Soo-jin.");
        $('#mep_' + (count - 1)).find('.mejs-play').find('button').trigger('click');

    } else if (count == 2) {
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
        $('.messageBox > span').html("Hello.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 3) {
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Today, we are traveling to Istanbul, Turkey. As you can see, Istanbul is a large bustling place.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 4) {
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
   
        $('.messageBox > span').html("But what you might not notice is that Istanbul is a city that sits in two continents: Asia and Europe. This is one of the reasons that this area has been so important to many groups over time.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 5) {
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("That's right, Soo-jin. The city you see today has been ruled by Persians, Greeks, Romans, Byzantines, and Ottomans in its long history. In fact, Istanbul is only the most recent name for this city. Long ago it was called Constantinople.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 6) {
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
   
        $('.messageBox > span').html("This city played a major role in so many important events throughout history. And on our journey today we are going to learn much more about it. We will explore the role Constantinople played in the spread of Christianity as well as Islam. We will learn about the effect the Crusades had on the city. And we will watch the development of the powerful Ottoman Empire.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 7) {
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html('But before we start, let&#39;s talk a little bit about studying history. Have you ever heard the quote, "the past is another country: they do things differently there"? It might sound a little corny, but it is very true.');
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 8) {
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
   
        $('.messageBox > span').html("That's right, Ali. When we study the past we have to remember that the people, places, and events we explore are as foreign to us as any foreign country is today.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 9) {
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("And, just like when attempting to understand any foreign culture, we have to keep reminding ourselves that people in the past did things differently because they understood things differently. Knowing how we would feel in those circumstances is not nearly as informative as discovering how they felt.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 10) {
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
   
        $('.messageBox > span').html("This doesn't mean that we can't be critical of people in the past. We have our own perspectives, informed by our cultures and context. Of course we will disagree with the way some historical personalities acted in the past.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 11) {
        $('.messageBox > div').removeClass('callout_woman').addClass("calloutman");
        $('.messageBox > span').html("Right! The important thing to remember is that we have to guard against allowing our own preconceived notions to distort the way we understand the past.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    } else if (count == 12) {
        $('.messageBox > div').removeClass('calloutman').addClass("callout_woman");
   
        $('.messageBox > span').html("That's exactly right, Ali. So, let's begin.");
        $('#mep_'+(count-1)).find('.mejs-play').find('button').trigger('click');
    }
}

