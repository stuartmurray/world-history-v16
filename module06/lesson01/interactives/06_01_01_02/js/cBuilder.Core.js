/* =====================================================================================
cBuilder - to bulit course structure and navigation
by Rajendran

dependency:
Jquery.js
======================================================================================== */

var cBuilder = {
    Utility:{},
    Page:{Index:null},
    Render:{doc:null,Sitemap:null},
    Event:{},
    Interactive:{ClickRevealStar:{},MultiChoice:{},SlideShow:{},Carousel:{},Dragdrop:{},Timeline:{},TrueorFalse:{},Dropdown:{}}
};  //namespace

var cBRender = cBuilder.Render; // shorthand var
var cBUtility = cBuilder.Utility; // shorthand var
var cBEvent = cBuilder.Event; // shorthand var
var cBPage = cBuilder.Page  // shorthand var
var cBInteractive = cBuilder.Interactive; // shorthand var



//******************** Render method block  *****************
//

cBRender.SetSitemapXml = function(sPath){
    // this is to get sitmap xml
   cBUtility.GetXml(sPath,
        function(data){ // on success
            cBRender.Sitemap = data;
        },
        function(){// before send
        }
   );    
};

cBRender.InitHome = function(){
   var oDoc = this.doc; // this.page.body
   
   this.SetSitemapXml("global/xml/sitemap.xml");
   $(oDoc).find(".themeswitch").click(function(){
        var oRoot = $(oDoc).find("#popup");
        $(oDoc).find("#overlay").show();
        cBEvent.OnModuleClick($(this),oRoot);
   });
   
};

cBRender.InitPage = function(){
    var oDoc, oPage, oNextPage, oPrevPage, sPage;
 
    oDoc = this.doc;
    sPage = cBUtility.GetCurrentPage(); // gets the correct naviagted page
    
    // Block 1 - this to set next prevoius navigation
    this.SetSitemapXml("../global/xml/sitemap.xml");
    oPage = cBUtility.FindPage(this.Sitemap,sPage);
    cBPage.Index = $(this.Sitemap).find("page").index(oPage);

    oNextPage = $(this.Sitemap).find("page:eq("+ (cBPage.Index+1) +")");
    oPrevPage = $(this.Sitemap).find("page:eq("+ (cBPage.Index-1) +")");
    if (oNextPage==undefined){
       oNextPage = $(this.Sitemap).find("sitemap");
    }
    if (oPrevPage==undefined){
       oPrevPage = $(this.Sitemap).find("sitemap");
    }

    $(oDoc).find("#nextPage a, #nextBtn a").attr("href",$(oNextPage).attr("href"));
    $(oDoc).find("#prevPage a, #prevBtn a").attr("href",$(oPrevPage).attr("href"));
    // Block 1 - end

     
    // each page slide show init 
    if($(oDoc).find(".Slideshow").length>0){
        $(oDoc).find(".Slideshow").Slideshow({interval:5000});
    }
    
    // each page font increase/ decrese init
    if($(oDoc).find("#increaseFont, #decreaseFont").length>0){
        $(oDoc).find("#increaseFont a, #decreaseFont a").click(function(){
             cBEvent.OnFontChange($(this), $(oDoc).find("div#margin"));
        });
    }
    
    //Accordian init
    if($(oDoc).find('dl.accordian > dt').length>0){
        $(oDoc).find('dl.accordian > dd').hide();
        $(oDoc).find('dl.accordian > dt').click(function(){
            cBEvent.OnAccordianClick($(this), $(oDoc).find("dl.accordian"));
        });
    }
	
    
	
	
	
     //Toggle Text Version/ inteaction init
	 
		

    if($(oDoc).find('.activeInteractive').length>0){

        $(oDoc).find(".toggleSwitch").click(function(){
            cBEvent.OnToggleSwitchClick($(this),$(oDoc).find('.activeInteractive')); 
			$(".jp-jplayer-in").jPlayer("stop");
			$(".activeInteractive .jp-jplayer").jPlayer("stop");
			$('.activeInteractive').find('.tooltip span').hide();
        });
		$(oDoc).find(".toggleSwitch").keypress(function(e){
			if(e.keyCode == '13'){
				return false;
			}
		});
        $(oDoc).find(".checkAnswer").click(function(){
            $(this).parent().find(".question").hide();
            $(this).parent().find(".answerPage").show();
            $(this).parent().find(".previousPage").show();
            $(this).hide();
        });
         $(oDoc).find(".previousPage").click(function(){
            $(this).parent().find(".question").show();
            $(this).parent().find(".answerPage").hide();
             $(this).parent().find(".checkAnswer").show();
            $(this).hide();
        });
		 
        $(oDoc).find(".checkAnswer1").click(function(){
            $(this).parent().find(".question1").hide();
            $(this).parent().find(".answerPage1").show();
            $(this).parent().find(".previousPage1, .nextPage2").show();
            $(this).hide();
        });
         $(oDoc).find(".previousPage1").click(function(){
            $(this).parent().find(".question1").show();
            $(this).parent().find(".answerPage1, .nextPage2").hide();
             $(this).parent().find(".checkAnswer1").show();
            $(this).hide();
        });
		 
         $(oDoc).find(".nextPage2").click(function(){
            $(this).parent().find(".question2").show();
            $(this).parent().find(".answerPage1, .previousPage1").hide();
             $(this).parent().find(".checkAnswer2").show();
            $(this).hide();
        });
        $(oDoc).find(".checkAnswer2").click(function(){
            $(this).parent().find(".question2").hide();
            $(this).parent().find(".answerPage2").show();
            $(this).parent().find(".previousPage2, .nextPage3").show();
            $(this).hide();
        });
         $(oDoc).find(".previousPage2").click(function(){
            $(this).parent().find(".question2").show();
            $(this).parent().find(".answerPage2, .nextPage3").hide();
             $(this).parent().find(".checkAnswer2").show();
            $(this).hide();
        });
		 
         $(oDoc).find(".nextPage3").click(function(){
            $(this).parent().find(".question3").show();
            $(this).parent().find(".answerPage2, .previousPage2").hide();
             $(this).parent().find(".checkAnswer3").show();
            $(this).hide();
        });
        $(oDoc).find(".checkAnswer3").click(function(){
            $(this).parent().find(".question3").hide();
            $(this).parent().find(".answerPage3").show();
            $(this).parent().find(".previousPage3, .nextPage4").show();
            $(this).hide();
        });
         $(oDoc).find(".previousPage3").click(function(){
            $(this).parent().find(".question3").show();
            $(this).parent().find(".answerPage3, .nextPage4").hide();
             $(this).parent().find(".checkAnswer3").show();
            $(this).hide();
        });
		 
         $(oDoc).find(".nextPage4").click(function(){
            $(this).parent().find(".question4").show();
            $(this).parent().find(".answerPage3, .previousPage3").hide();
             $(this).parent().find(".checkAnswer4").show();
            $(this).hide();
        });
        $(oDoc).find(".checkAnswer4").click(function(){
            $(this).parent().find(".question4").hide();
            $(this).parent().find(".answerPage4").show();
            $(this).parent().find(".previousPage4, .nextPage5").show();
            $(this).hide();
        });
         $(oDoc).find(".previousPage4").click(function(){
            $(this).parent().find(".question4").show();
            $(this).parent().find(".answerPage4, .nextPage5").hide();
             $(this).parent().find(".checkAnswer4").show();
            $(this).hide();
        });
		 

         $(oDoc).find(".nextPage5").click(function(){
            $(this).parent().find(".question5").show();
            $(this).parent().find(".answerPage4, .previousPage4").hide();
             $(this).parent().find(".checkAnswer5").show();
            $(this).hide();
        });
        $(oDoc).find(".checkAnswer5").click(function(){
            $(this).parent().find(".question5").hide();
            $(this).parent().find(".answerPage5").show();
            $(this).parent().find(".previousPage5").show();
            $(this).hide();
        });
         $(oDoc).find(".previousPage5").click(function(){
            $(this).parent().find(".question5").show();
            $(this).parent().find(".answerPage5").hide();
             $(this).parent().find(".checkAnswer5").show();
            $(this).hide();
        });


		 
        $(oDoc).find(".printText").click(function(){
            $(oDoc).find("#printBox").append($(this).closest(".textVersion").clone()); 
            $(oDoc).find("#printBox").show();
            $(oDoc).find("#overlay").show(); 
            $(oDoc).find("#window").hide();
            $(oDoc).find("#printBox").find(".printText, .checkAnswer, .previousPage").hide();
            window.print();
            $(oDoc).find("#printBox").find(".close").click(function(){
                $(this).closest("#overlay").hide();
                $(this).closest("#printBox").hide();
                $(this).closest("#printBox").find(".textVersion").remove();
                $(oDoc).find("#window").show();
            });
        });
        $(oDoc).find(".swfObject").each(function(){
            cBRender.InitInteractive($(this).find("a.interactive"),$(this),function(){ //interaction init success
			   cBRender.InitToolTip();
            });
        });
		//cBRender.InitToolTip();
       
       
    }
    else{
         cBRender.InitToolTip();
    }
   
   //Pop up init
    if($(oDoc).find('.pop, .journalPop').length>0){
        $(oDoc).find('.pop, .journalPop').live('click',function(){
            var location, height, isExternalLink;
    
            location = $(this).attr("href");
            if($(self).attr("data-height")){
                height = $(self).attr("data-height");
            }else if($(self).attr("rel")){
                height = $(self).attr("rel");
            }else{
                height = 600;
            }
			
			isExternalLink = (location.indexOf("http://") != -1)? "yes" : "no" ;
			window.open(location,'popUp','toolbar='+isExternalLink+',location='+isExternalLink+',directories='+isExternalLink+', status=yes,menubar='+isExternalLink+',scrollbars=yes,resizable=yes,width=700,height='+height);
            return false;
        });
    }
    
    

};

cBRender.InitToolTip = function(){
    var oDoc = this.doc;
    //Tool Tip init
    if($(oDoc).find('.tooltip').length>0){
        $(oDoc).find('.tooltip span').hide();
        $(oDoc).find('.tooltip').each(function(){
            $(this).find("span").append($(document.createElement("a")).addClass("toolTipClose")); 
            $(this).click(function(){
				// Glossary popup audio should stop when popup is closed,(audio is still heard when another glossary popup is opened)
				$(oDoc).find('.jQPlayer').each(function(index){
					var sId = $(this).find("div:eq(0)").attr('id');
					$('#'+sId).jPlayer("pause");
				});//
                $(oDoc).find('.tooltip span').hide();
				initJPlayerAudio($(this).find(".jQPlayer"));
                $(this).find("span").show();
				
                $(this).find(".toolTipClose").click(function(e){
                    e.stopPropagation();
                    $(this).closest(".tooltip").find("span").hide();
					var sId = $(this).closest(".tooltip").find(".jQPlayer").find("div:eq(0)").attr('id');
					stopAudio(sId);
					//$("#"+sId).jPlayer("destroy");
					//initJPlayerAudio($(this).closest(".tooltip").find(".jQPlayer"));
                });
            });         
        });
    }
    
    // jPlayer audio init
    if($(oDoc).find('.jQPlayer').length>0){
        $(oDoc).find('.jQPlayer').each(function(index){
            var obj = $(this);
			t = setTimeout(function(){
				var sAudio = obj.attr("href");
				obj.replaceWith(cBUtility.GetjPlayerSkin(index,sAudio));
				cBRender.InitjPlayer("jquery_jplayer_"+(index+1),sAudio,"jp_container_"+ (index+1));  
			},1000);     
        }); // each end
    }

};

function initJPlayerAudio(cur_obj){
	var obj = cur_obj;
	var idTmp = obj.find("div:eq(0)").attr('id');
	var index = idTmp.replace('jquery_jplayer_','');
	index = parseInt(index)-1;
	//t = setTimeout(function(){
		var sAudio = obj.attr("href")?obj.attr("href"):obj.attr("audio");
		obj.replaceWith(cBUtility.GetjPlayerSkin(index,sAudio));
		cBRender.InitjPlayer("jquery_jplayer_"+(index+1),sAudio,"jp_container_"+ (index+1));  
	//},1000);     
}


cBRender.InitjPlayer = function(sID, sAudio, sAncestor){
    $("#"+sID).jPlayer({
		ready: function (event) {
			$(this).jPlayer("setMedia", {
				mp3:sAudio
			});
		},
		play: function() {
			$(this).jPlayer("pauseOthers");
			//$(this).jPlayer("play");
		},
		ended: function(){
			//initJPlayerAudio($('#'+sID).parent());
		},
		swfPath: "../global/js",
		supplied: "mp3",
		preload: "metadata",
		cssSelectorAncestor: "#"+sAncestor,
		wmode: "window"
	});
}

cBRender.InitInteractive=function(self,root,callback){
    var oInteractive, sLoadScript;

    cBUtility.GetHtml($(self).attr("href"),
        function(data){ //on success
            oInteractive=data;
            $(root).append($(document.createElement("div")).addClass("Buffer").css("display","none").html(oInteractive));
            $(root).find(".iHead a.iStyle").each(function(){
                var oLink = document.createElement("link");
                oLink.type = "text/css";
                oLink.rel = "stylesheet";
                oLink.href = $(this).attr("href");
                document.getElementsByTagName("head")[0].appendChild(oLink);
            });
            $(root).find(".iHead a.iScript").each(function(index){
                 sLoadScript = $(this).attr("onload")||$(this).attr("afterload");
                 cBUtility.GetScript($(this).attr("href"),
                    function(){
                        if($(root).find(".iHead a.iScript").length==(index+1)){
                            $(root).find(".iHead").remove();
                            var oiBody = $(root).find(".iBody").html();
                            $(root).empty();
                            $(root).append(oiBody);
                            if(sLoadScript!='' && sLoadScript!=null && sLoadScript!=undefined){
                                eval(sLoadScript);
                                return callback();
                            }
                        }
                    });
            });
        },
        function(){ // before send
            $(root).empty();
            $(root).append($(document.createElement("div")).attr("id","appLoader"));
        });
    
};

// Event method block end
//_________________________

//******************** Event method block *****************
// 

// home main module click
cBEvent.OnModuleClick = function(self,root){
    var iModuleID, list, li;
    
    iModuleID=$(self).attr("id").replace('mod_','');
    
    // render moudle lessons
    $(root).find('#lessons').empty();
    $(cBRender.Sitemap).find("module:eq("+ (iModuleID-1) +")").each(function(index){
        list = $(document.createElement("ul")).attr('id','mod'+iModuleID); // module wise list
        cBPage.Index = $(cBRender.Sitemap).find("page").index($(this).find("page:first")); // assign index of page in sitemap
        $(this).find("lesson").each(function(index){
            if($(this).find("page").length>0){
                // <li title={lesson.title} href={page:first.href}
                li = $(document.createElement("li")).append($(document.createElement("a")).attr("href", $(this).find("page:first").attr("href").replace("../","")).text($(this).attr("title")));
               
            }
            else{
                li = $(document.createElement("li")).append($(document.createElement("a")).attr("href", "#").text($(this).attr("title")));
            }
             list.append(li);
        });

        $(root).find('#lessons').html(list);
    });
  
    $(root).find("#mod" + iModuleID).show();
    $(root).show();
};

// Update Text font Size
var isDefaultFont = true;
var attainMaxsize = false; 
var attainMinsize = false; 

cBEvent.OnFontChange = function(self,root){
    var currentFontSizeNum, newFontSize;
    
    currentFontSizeNum = parseFloat($(root).css('font-size'), 10);
    if($(self).parent().attr("id") == "increaseFont"){
		if((isDefaultFont == false || isDefaultFont == "false") && (attainMaxsize == false || attainMaxsize == "false")){
        	newFontSize = currentFontSizeNum*1.0;
			$('.cpation_font').css('font-size','12px');
			isDefaultFont = true;
		}else{
			newFontSize = currentFontSizeNum*1.2;
			$('.cpation_font').css('font-size','14px');
			isDefaultFont = false;
			attainMaxsize = true;
		}
		attainMinsize = false;
		
    }else{
		if((isDefaultFont == false || isDefaultFont == "false") && (attainMinsize == false || attainMinsize == "false")){
        	newFontSize = currentFontSizeNum*1.0;
			$('.cpation_font').css('font-size','12px');
			isDefaultFont = true;
			attainMinsize = false;
		}else{
			newFontSize = currentFontSizeNum*0.8;
			$('.cpation_font').css('font-size','10px');
			isDefaultFont = false;
			attainMinsize = true;
		}
		attainMaxsize = false;
    }
	$(root).find(".swfObject table, .swfObject p, .swfObject ul, .swfObject ol").each(function(){
		$(this).addClass('nofontChange');
	});
	
	$(root).find("p:not('.nofontChange'), #pageCrumbs, table:not('.nofontChange'), .journalButton, ul:not('.nofontChange'), ol:not('.nofontChange'), .infoBox:not('.swfObject .infoBox')").css('font-size', newFontSize);
	
	$(root).find(".swfObject table, .swfObject p, .swfObject ul, .swfObject ol").each(function(){
		$(this).removeClass('nofontChange');
	});
	
}


cBEvent.OnAccordianClick = function(self,root){
    $(self).removeClass('on');
    $(self).parent().find('dd').find('.tooltip').css('position','static');
    $(self).parent().find('dd').slideUp('normal',function(){
        $(this).find('.tooltip').css('position','relative');
    });
    if($(self).next().is(':hidden') == true) {
        $(self).addClass('on');
        $(self).next().find('.tooltip').css('position','static');
        $(self).next().slideDown('normal',function(){
            $(this).find('.tooltip').css('position','relative');
        });
    }
}; 

cBEvent.OnPopupClick = function(self,root){
    var location, height, isExternalLink;
    
    location = $(this).attr("href");
	$(oDoc).find("#overlay").hide();
    if($(self).attr("data-height")){
        height = $(self).attr("data-height");
    }else if($(self).attr("rel")){
        height = $(self).attr("rel");
    }else{
        height = 600;
    }
    isExternalLink = (location.indexOf("http://") != -1)? "yes" : "no" ;
    root.open(location,'popUp','toolbar='+isExternalLink+',location='+isExternalLink+',directories='+isExternalLink+', status=yes,menubar='+isExternalLink+',scrollbars=yes,resizable=yes,width=700,height='+height);
   //return false;
};

cBEvent.OnToggleSwitchClick = function(self,root){
    $(self).text(($(self).text() == "Interactive")? "Text Version" : "Interactive");
    $(self).parent().children("div.textVersion").toggleClass("notVisible");
	
    if ( $(self).parent().children("div.swfObject").length > 0 ) {
	    $(self).parent().children("div.swfObject").toggleClass("notVisible");
    } else if ( $(self).parent().children("iframe").length > 0 ) {
	    $(self).parent().children("iframe").toggleClass("notVisible");
    }
    //return false;
};

// Event method block end
//_________________________


//******************** Utility method block  *****************
//


cBUtility.GetXml=function(sFilePath,callback,beforeCallback){ // #ajax to get xml
    var oData;
    $.ajax({
        async: false,
        cache: false,
		type: "GET",
        url: sFilePath,
        beforeSend:function(){return beforeCallback();},
        dataType: (jQuery.browser.msie) ? 'text' : 'xml',
        success: function(data){
            oData=cBUtility.GetSupportableFormat(data);
            return callback(oData);
        },
        error: function(){
            oData=null;
        }
      });
      
      return oData;
};

cBUtility.GetHtml=function(sFilePath,callback,beforeCallback){ // #ajax to get html
    var oData;
    $.ajax({
        async: false,
        type: "GET",
        url: sFilePath,
        beforeSend:function(){return beforeCallback();},
        dataType: 'html',
        success: function(data){
            oData=data;
            return callback(oData);
        },
        error: function(){
            oData=null;
        }
      });
      
      return oData;
};

cBUtility.GetScript=function(sFilePath,callback){ // #ajax jaquery getScript has no before send
    $.ajax({
        type: "GET",
        url: sFilePath,
        cache: false,
        async: true,
        dataType: 'script',
        success: function(){
            return callback();
        },
        error: function(jqXHR, textStatus, errorThrown ){
            return null;
        }
      });
};

cBUtility.GetSupportableFormat= function(data){
    var xml; 
    if ( typeof data == 'string') { 
        xml = new ActiveXObject( 'Microsoft.XMLDOM'); 
        xml.async = false; 
        xml.loadXML( data); 
    } else { 
        xml = data; 
    } 
    return xml;
};

cBUtility.ZeroPad=function(num,count){
   var numZeropad = String(num);
	while(numZeropad.length < count) {
		numZeropad = "0" + numZeropad;
	}
	return numZeropad;
};

cBUtility.GetCurrentPage = function(){
    var sCurrectURL,sPage;
    sCurrectURL = location.href;
    sPage = sCurrectURL.substring(sCurrectURL.lastIndexOf("/")+1,sCurrectURL.length);
    return sPage;
};

cBUtility.FindPage = function(oSitemap,sFilter){
    var oPage;
    $(oSitemap).find("page").each(function(){
        if($(this).attr("href").indexOf(sFilter)>-1){
            oPage= $(this);
            return;
        }
    });
    return oPage;
};

cBUtility.GetjPlayerSkin = function(index,sAudio){
    var oPlayer;
 
    oPlayer = $(document.createElement("div")).addClass("jQPlayer").attr('audio',sAudio)
              .html($(document.createElement("div")).addClass("jp-jplayer").attr("id","jquery_jplayer_"+ (index+1)))
              .append($(document.createElement("div")).addClass("jp-audio").attr("id","jp_container_"+ (index+1))
              .html($(document.createElement("div")).addClass("jp-type-single")
              .html($(document.createElement("div")).addClass("jp-gui").addClass("jp-interface")
              .html($(document.createElement("div")).addClass("jp-gui").addClass("jp-controls")
              .html($(document.createElement("a")).addClass("jp-play").attr("tabindex","1").attr("href","javascript:;").text("play"))
              .append($(document.createElement("a")).addClass("jp-pause").attr("tabindex","1").attr("href","javascript:;").text("pause"))
              ))));
    return oPlayer;
}

function stopAudio(sId){
	$("#"+sId).jPlayer('stop');
}

$(document).ready(function(){
	var printBTN = $(document.createElement('li')).attr('id', 'printBTN').append($(document.createElement('a')).attr('href', '#').bind('click', printDoc).attr('title', 'Print').append($(document.createElement('span')).text('Print')));
	$('#notesPage').after(printBTN);
});

function printDoc(evt) {
	window.print();
}


// Utility method block end
//_________________________