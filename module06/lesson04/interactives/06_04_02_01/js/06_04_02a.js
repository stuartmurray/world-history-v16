﻿$(document).ready(function () {
    $('.panel').each(function () {
        $(this).find('.card').each(function (e) {
            if (e >= 3) {
                $(this).remove();

            }
        });
    });
    $('.hiddentool').hide();
    $(".aud").bind('click', function () {
        $('.hiddentool').show();
        $('.close2').bind('click', function () {
            $('.hiddentool').stop();
            if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
                $('#mep_1 button').trigger('click');
            }
        });
    });

    $('.carousel-prev').bind('click', function () {
        $('.hiddentool').hide();

        if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {

            $('#mep_1 button').trigger('click');
        }
    });

    $('.carousel-next').bind('click', function () {
        $('.hiddentool').hide();

        if ($('#mep_1 .mejs-button').hasClass('mejs-pause')) {
            $('#mep_1 button').trigger('click');

            $('.hiddentool').hide();
        }
    });

});

$('#close2').click(function () {
    $('#hiddentool').hide();

});

var check = 0;
$('._audio-tip').click(function () {
    check = 1;
    for (var i = 0; i < $("audio").length; i++) {
        $("audio")[i].pause();
    }
    $('.tooltip').hide();
    $('#tooltip_' + $(this).attr('id')).show();
    //    console.log('show');
});
$(document).click(function (e) {
    if (check == 1) {
        check = 0;
        return;
    }
    var $target = $(e.target);
    if ($target.closest('.tooltip').length == 0) {
        //        console.log('hide');
        for (var i = 0; i < $("audio").length; i++) {
            $("audio")[i].pause();
        }
        $('.tooltip').hide();
    }
});